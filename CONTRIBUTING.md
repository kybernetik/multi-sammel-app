Any commit to this repository implicitely allows Sammel-App maintainers to 
relicense this software to any OSI-approved license, without prior consent.
This is necessary for iOS releases.
