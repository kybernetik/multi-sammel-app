# Berliner Sammelapp
Strengthen direct democracy in Berlin! The Berliner Sammelapp supports citizens' signature campaigns for referendums!

With the app you can see upcoming events, register your participation, chat with other participants and report back afterwards. Whether it’s a collection drive, a workshop, or an informational event – the app lets you define, about what kind of actions in what area at what time you’d like to hear. Submit your own action, stay up to date with the campaign’s developments or use the FAQ to brush up on common arguments and good answers to the most important questions about the current campaigns in Berlin.

more info at https://berliner-sammelapp.de/

## Setting up project
The Sammelapp uses Flutter for the app and a docker container for running the backend via a [Wildfly Application Server](https://www.wildfly.org/).

You can run the app in demo mode without accessing a backend.

### Setting up Flutter
- clone the project locally
- install the [IntellijJ IDEA Community Edition](https://www.jetbrains.com/idea/) as your IDE
- follow these instructions for setting up Flutter: https://docs.flutter.dev/get-started/editor?tab=androidstudio
- open the project (notice it is subdived into `sammel_app` for the app and `sammel_server` for the server)
- run `flutter doctor` and resolve anything that's missing
- open the `pubspec.yam` file and run `Pub get` (on IDEA this should be displayed in the top right)
- ensure demo mode is active in `Provisioning.dart`: `const Mode mode = Mode.DEMO;`
- start an iOS simulator or Android emulator; if you're on OSX, this will require downloading XCode and a bunch of associated tools; if you're running Android, you need to set one up via `Tools > Android > AVD Manager`
- run the app by running the "Starte App im Emulator" run configuration

### Setting up the server
- install the Docker plugin and the Database Navigator plugin for IDEA
- run the `Baue und starte alles` run configuration
- wait, have a look at the `Services` tab to see what's going on
- check that the server is running at https://localhost:18443/service/health (you'll likely get a warning about a self-signed cert)
- use the Database Navigator to connect to jdbc:mysql://localhost:3306, user `root`, password `password`


Set `const Mode mode = Mode.LOCAL;` to connect the app to your local server.


## Lizenz

Der Code und alle Inhalte dieses Projekts, inklusive Bilder und Dokumentationen, sowie des Codes und der Docker-Images sind Open-Source und können frei und unentgeltlich genutzt und weiterentwickelt werden. Die  weiterentwickelten und abgeleiteten Werke müssen dabei jedoch unter den selben Bedingungen lizenisert werden und der Quellcode öffentlich zugänglich gemacht werden.

Die Inhalte stehen unter der [GNU Public License](https://www.gnu.org/licenses/gpl-3.0.de.html).

Folgende Ausnahmen bestehen für die freie Nutzung der Inhalte, des Codes und der App: Der Code und alle weiteren Inhalte dieses Projekts können nicht genutzt werden für politische Initiativen die sexistische, rassistische, antisemitische, nationalistische, anti-soziale, umweltfeindliche oder militärische Ziele verfolgen. Um Sicherheit über die freie Verwendung der Inhalte dieses Projekts zu haben, empfiehlt es sich mit den Entwickler*innen in Kontakt zu treten unter [dwesammelapp@googlemail.com](mail:dwesammelapp@googlemail.com).
