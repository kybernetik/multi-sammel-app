class Evaluation {
  int? terminId;
  int? teilnehmer;
  int? unterschriften;
  int? bewertung;
  double? stunden;
  List<String> ortTypen = [];
  String? verbesserung = '';
  String? beschreibung = '';
  String? sonstiges = '';
  bool ausgefallen = false;
  bool fuerAlle = false;
  bool? schaetzung = false;
  bool inBewegung = false;

  Evaluation(
      this.terminId,
      this.teilnehmer,
      this.fuerAlle,
      this.schaetzung,
      this.unterschriften,
      this.stunden,
      this.ortTypen,
      this.inBewegung,
      this.bewertung,
      this.beschreibung,
      this.verbesserung,
      this.sonstiges);

  Evaluation.ausgefallen() : ausgefallen = true;

  Map<String, dynamic> toJson() => {
        'termin_id': terminId,
        'teilnehmer': teilnehmer,
        'unterschriften': unterschriften,
        'bewertung': bewertung,
        'stunden': stunden,
        'ort_typen': ortTypen,
        'beschreibung': beschreibung,
        'verbesserung': verbesserung,
        'sonstiges': sonstiges,
        'in_bewegung': inBewegung,
        'schaetzung': schaetzung,
        'fuer_alle': fuerAlle,
        'ausgefallen': ausgefallen
      };
}
