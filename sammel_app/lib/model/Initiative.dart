import 'dart:ui';

class Initiative {
  int id;
  String longName;
  String shortName;
  Color primaryColor;
  Color? secondaryColor;
  String phase;
  String websiteUrl;
  String description;
  String? registerSignaturesUrl;

  Initiative(
      this.id,
      this.longName,
      this.shortName,
      this.primaryColor,
      this.secondaryColor,
      this.phase,
      this.websiteUrl,
      this.description,
      this.registerSignaturesUrl);

  Map<String, dynamic> toJson() => {
        'id': id,
        'longName': longName,
        'shortName': shortName,
        'primaryColor': primaryColor.value.toRadixString(16),
        'secondaryColor': secondaryColor?.value.toRadixString(16),
        'phase': 'phase',
        'websiteUrl': 'websiteUrl',
        'description': 'description',
        'registerSignaturesUrl': 'registerSignaturesUrl'
      };

  Initiative.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        longName = json['longName'],
        shortName = json['shortName'],
        primaryColor = Color(int.parse(json['primaryColor'])),
        secondaryColor = json['secondaryColor'] != null
            ? Color(int.parse(json['secondaryColor']))
            : null,
        phase = json['phase'],
        websiteUrl = json['websiteUrl'],
        description = json['description'],
        registerSignaturesUrl = json['registerSignaturesUrl'];
}
