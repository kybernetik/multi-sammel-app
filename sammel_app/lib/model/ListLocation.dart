import 'package:sammel_app/model/Initiative.dart';

class ListLocation {
  int? id;
  String? name;
  String? street;
  String? number;
  String? description;
  String? contact;
  double latitude;
  double longitude;
  List<Initiative> initiatives;

  ListLocation(this.id, this.name, this.street, this.number, this.description,
      this.contact, this.latitude, this.longitude, this.initiatives);

  ListLocation.fromJson(jsonListLocation, List<Initiative> initiatives)
      : id = jsonListLocation['id'],
        name = jsonListLocation['name'],
        street = jsonListLocation['street'],
        number = jsonListLocation['number'],
        description = jsonListLocation['description'],
        contact = jsonListLocation['contact'],
        latitude = jsonListLocation['latitude'],
        longitude = jsonListLocation['longitude'],
        initiatives = (jsonListLocation['initiativenIds'] as List?)
                ?.map((initiativeId) =>
                    initiatives.firstWhere((ini) => ini.id == initiativeId))
                .toList() ??
            [];

  Map<dynamic, dynamic> toJson() => {
        'id': id,
        'latitude': latitude,
        'longitude': longitude,
        'street': street,
        'number': number,
        'name': name,
        'description': description,
        'contact': contact,
        'initiativenIds':
            initiatives.map((initiative) => initiative.id).toList()
      };
}
