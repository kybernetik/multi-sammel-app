import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:sammel_app/Provisioning.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/model/VisitedHouse.dart';
import 'package:sammel_app/model/ListLocation.dart';
import 'package:sammel_app/model/Placard.dart';
import 'package:sammel_app/model/Termin.dart';
import 'package:sammel_app/routes/ArgumentsDialog.dart';
import 'package:sammel_app/routes/InitiativePickerDialog.dart';
import 'package:sammel_app/routes/ListLocationEditor.dart';
import 'package:sammel_app/services/ArgumentsService.dart';
import 'package:sammel_app/services/ErrorService.dart';
import 'package:sammel_app/services/GeoService.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/PlacardsService.dart';
import 'package:sammel_app/services/VisitedHouseView.dart';
import 'package:sammel_app/services/VisitedHousesService.dart';
import 'package:sammel_app/shared/AttributionPlugin.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';
import 'package:sammel_app/shared/NoRotation.dart';
import '../shared/DistanceHelper.dart';
import 'VisitedHouseEditor.dart';
import 'MapActionDialog.dart';
import 'PlacardDialog.dart';
import 'EditVisitedHouse.dart';

class ActionMap extends StatefulWidget {
  final List<Termin> termine;
  final List<ListLocation> listLocations;
  final int? myUserId;
  final Function(Termin) isMyAction;
  final Function(Termin) isPastAction;
  final Function(Termin) iAmParticipant;
  final Function(Termin) openActionDetails;
  final Function(LatLng point) switchToActionCreator;
  final Function(LatLng point) switchToListLocationCreator;
  final Function(ListLocation) afterListLocationEdit;
  late final MapController mapController;

  // no better way yet: https://github.com/dart-lang/sdk/issues/4596
  static falseFunction(Termin _) => false;

  static emptyFunction(_) {}

  ActionMap({
    Key? key,
    this.termine = const [],
    this.listLocations = const [],
    this.myUserId,
    this.isMyAction = falseFunction,
    this.isPastAction = emptyFunction,
    this.openActionDetails = emptyFunction,
    this.iAmParticipant = emptyFunction,
    this.switchToActionCreator = emptyFunction,
    this.switchToListLocationCreator = emptyFunction,
    this.afterListLocationEdit = emptyFunction,
    mapController,
  }) : super(key: key) {
    this.mapController = mapController ?? MapController();
  }

  @override
  ActionMapState createState() => ActionMapState();
}

class ActionMapState extends State<ActionMap> {
  ActionMapState();

  var locationPermissionGranted = false;
  List<Placard> placards = [];
  List<Marker> placardMarkers = [];
  List<Marker> actionMarkers = [];
  List<Marker> listLocationMarkers = [];
  late AbstractPlacardsService placardService;
  List<Initiative> allInitiatives = [];
  var initialized = false;
  var createdMarkers = false;
  List<Polygon> housePolygons = [];

  final int placardMinZoom = 15; // minimum zoom level for showing placards

  double lastZoomLevel = 0;

  @override
  Widget build(BuildContext context) {
    if (!initialized) {
      placardService = Provider.of<AbstractPlacardsService>(context);
      loadPlacards();

      // We need the initiatives to check if we want to show the option to add an iniative
      // to a list location
      Provider.of<AbstractInitiativesService>(context)
          .loadInitiatives()
          .then((inis) => setState(() => allInitiatives = inis));

      Geolocator.checkPermission().then((value) => {
            if (value != LocationPermission.deniedForever)
              {Geolocator.requestPermission()}
          });
    }

    this.placardMarkers = generatePlacardMarkers();
    this.actionMarkers = generateActionMarkers();
    this.listLocationMarkers = generateListLocationMarkers();

    List<MapPlugin> plugins = [];
    plugins.add(AttributionPlugin());
    plugins.add(LocationMarkerPlugin());
    plugins.add(MarkerClusterPlugin());
    this.housePolygons = generateVisitedHousePolygons();
    List<LayerOptions> layers = [
      TileLayerOptions(
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: ['a', 'b', 'c']),
      LocationMarkerLayerOptions(),
      PolygonLayerOptions(polygons: this.housePolygons, polygonCulling: true),
      MarkerLayerOptions(
          markers: <Marker>[]
            ..addAll(this.actionMarkers)
            ..addAll(this.placardMarkers)),
      MarkerClusterLayerOptions(
        maxClusterRadius: 120,
        size: Size(80, 80),
        fitBoundsOptions: FitBoundsOptions(
          padding: EdgeInsets.all(50),
        ),
        markers: this.listLocationMarkers,
        polygonOptions: PolygonOptions(
            borderColor: Colors.blueAccent,
            color: Colors.black12,
            borderStrokeWidth: 3),
        builder: (context, markers) {
          return TextButton.icon(
              key: Key('list location cluster marker'),
              style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                    EdgeInsets.all(0)),
              ),
              onPressed: null,
              label: Text(
                markers.length.toString(),
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              icon: Image.asset('assets/images/Soliort_black.png',
                  width: 30, height: 30));
        },
      ),
    ];
    layers.add(AttributionOptions());

    var flutterMap = FlutterMap(
      key: Key('action map map'),
      options: MapOptions(
        onTap: (var tapPosition, LatLng point) => mapTap(point),
        onLongPress: (var tapPosition, LatLng point) => mapAction(point),
        plugins: plugins,
        center: LatLng(geo.initCenterLat, geo.initCenterLong),
        swPanBoundary: LatLng(geo.boundLatMin, geo.boundLongMin),
        nePanBoundary: LatLng(geo.boundLatMax, geo.boundLongMax),
        zoom: geo.initZoom,
        interactiveFlags: noRotation,
        maxZoom: geo.zoomMax,
        minZoom: geo.zoomMin,
        onPositionChanged: (position, _) =>
            widget.mapController.onReady.then((_) => {
                  if ((lastZoomLevel < placardMinZoom &&
                          widget.mapController.zoom > placardMinZoom) ||
                      (lastZoomLevel > placardMinZoom &&
                          widget.mapController.zoom < placardMinZoom))
                    {
                      setState(() {
                        // Only generate new markers if zoom level changed for placards

                        this.lastZoomLevel = widget.mapController.zoom;
                        this.placardMarkers = generatePlacardMarkers();
                      })
                    }
                }),
      ),
      layers: layers,
      mapController: widget.mapController,
    );
    initialized = true;
    return flutterMap;
  }

  loadPlacards() {
    placardService.loadPlacards([]).then(
        (placards) => setState(() => this.placards = placards));
  }

  void deletePlacard(Placard placard) {
    placardService.deletePlacard(placard.id!);
    setState(() => placards.remove(placard));
  }

  void takeDownPlacard(Placard placard) {
    placardService.takeDownPlacard(placard.id!);
    setState(() => placards.remove(placard.abgehangen = true));
  }

  List<Polygon> generateVisitedHousePolygons() {
    if (initialized && widget.mapController.zoom > 14) {
      //return widget.visitedHouses;

      BoundingBox bbox = BoundingBox(
          widget.mapController.bounds!.south,
          widget.mapController.bounds!.west,
          widget.mapController.bounds!.north,
          widget.mapController.bounds!.east);
      VisitedHouseView houseView =
          Provider.of<AbstractVisitedHousesService>(context, listen: false)
              .getVisitedHousesInArea(bbox);
      return houseView.buildDrawablePolygonsFromView();
    }
    return [];
  }

  mapTap(LatLng point) async {
    VisitedHouse? vh =
        await Provider.of<AbstractVisitedHousesService>(context, listen: false)
            .getVisitedHouseOfPoint(point, false);

    if (vh != null) {
      var showDistanceInMeter = 100.0;
      var lngDiff = DistanceHelper.getLongDiffFromM(point, showDistanceInMeter);
      var latDiff = DistanceHelper.getLatDiffFromM(point, showDistanceInMeter);
      BoundingBox bbox = BoundingBox(
          point.latitude - latDiff,
          point.longitude - lngDiff,
          point.latitude + latDiff,
          point.longitude + lngDiff);
      var visitedHouseView =
          Provider.of<AbstractVisitedHousesService>(context, listen: false)
              .getVisitedHousesInArea(bbox);
      visitedHouseView.selectedHouse = SelectableVisitedHouse.clone(
          SelectableVisitedHouse.fromVisitedHouse(vh, selected: true));
      await showEditVisitedHouseDialog(
          context: context,
          visitedHouseView: visitedHouseView,
          currentZoomFactor: widget.mapController.zoom);
      setState(() {
        this.housePolygons = generateVisitedHousePolygons();
      });
    }
  }

  Future openListLocationDialog(ListLocation listLocation) async =>
      await showDialog(
          context: context,
          builder: (context) => SimpleDialog(
                  title: Text(
                      listLocation.name ??
                          '${listLocation.street ?? ''} ${listLocation.number ?? ''}',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: CampaignTheme.black)),
                  key: Key('list location info dialog'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  contentPadding: EdgeInsets.all(10.0),
                  children: <Widget>[
                    // If street and number is same as name we don't want to render it here
                    Text('${listLocation.street} ${listLocation.number}' !=
                            listLocation.name
                        ? 'Adresse: ${listLocation.street} ${listLocation.number}\n'
                        : ''),
                    Text('Hier liegen öffentliche Unterschriften-Listen für folgende Initiativen aus:\n')
                        .tr(),
                    for (var ini in listLocation.initiatives)
                      Text('- ${ini.longName}\n'),
                    if (listLocation.description != null)
                      Text('Beschreibung: ${listLocation.description!}\n'),
                    if (listLocation.contact != null)
                      Text('Kontakt: ${listLocation.contact!}\n'),
                    Text(
                      'Du kannst selbst Unterschriften-Listen an öffentlichen Orten auslegen, z.B. in Cafés, Bars oder Läden. '
                      'Wichtig ist, dass du die ausgefüllten Listen regelmäßig abholst.\n'
                      'Frage doch mal die Betreiber*innen deines Lieblings-Spätis!\n',
                    ).tr(),
                    TextButton(
                        key: Key('join action button'),
                        style: CampaignTheme.yesButtonStyle,
                        child: Text('Soli-Ort bearbeiten').tr(),
                        onPressed: () {
                          Navigator.pop(context);
                          editListLocationDialog(context, listLocation);
                        })
                  ]));

  Future editListLocationDialog(
          BuildContext context, ListLocation listLocation) async =>
      await showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) {
            return SimpleDialog(
                contentPadding: EdgeInsets.zero,
                title: Text("Soli-Ort bearbeiten",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: CampaignTheme.black)),
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.7,
                    child: ListLocationEditor(
                        initListLocation: listLocation,
                        onFinish: (ListLocation listLocation) {
                          widget.afterListLocationEdit(listLocation);
                          Navigator.pop(context);
                          openListLocationDialog(listLocation);
                        },
                        key: Key('list location editor')),
                  )
                ]);
          });

  Future openPlacardDialog(Placard placard, bool mine) async {
    if (placard.id == null) return;

    PlacardDialogAction action =
        await showPlacardDialog(context, placard, mine);

    if (action == PlacardDialogAction.DELETE) deletePlacard(placard);
    if (action == PlacardDialogAction.TAKE_DOWN) takeDownPlacard(placard);
  }

  createNewPlacard(LatLng point) async {
    Initiative initiative = await showInitiativePickerDialog(context);

    if (widget.myUserId == null) {
      ErrorService.pushError('Plakat kann nicht erstellt werden',
          'Die App hat noch kein Benutzer*inprofil für dich erzeugt. Versuche es bitte später nochmal');
      return;
    }

    final geoData = await Provider.of<GeoService>(context, listen: false)
        .getDescriptionToPoint(point);

    final placard = await placardService.createPlacard(Placard(
        null,
        point.latitude,
        point.longitude,
        geoData.fullAdress,
        widget.myUserId!,
        false,
        initiative));
    if (placard != null) {
      setState(() => placards.add(placard));
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(
              'Neues Plakat an der Adresse "${placard.adresse}" eingetragen'
                  .tr(),
              style: TextStyle(color: Colors.black87)),
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: 5),
          backgroundColor: Color.fromARGB(220, 255, 255, 250)));
    }
  }

  mapAction(LatLng point) async {
    MapActionType? chosenAction = await showMapActionDialog(context);

    if (chosenAction == MapActionType.NewAction) {
      widget.switchToActionCreator(point);
    }
    if (chosenAction == MapActionType.NewListLocation) {
      widget.switchToListLocationCreator(point);
    }
    if (chosenAction == MapActionType.NewPlacard) createNewPlacard(point);
    if (chosenAction == MapActionType.NewVisitedHouse)
      createNewVisitedHouse(point);
  }

  void createNewVisitedHouse(LatLng point) async {
    var showDistanceInM = 100.0;
    var lngDiff = DistanceHelper.getLongDiffFromM(point, showDistanceInM);
    var latDiff = DistanceHelper.getLatDiffFromM(point, showDistanceInM);
    BoundingBox bbox = BoundingBox(
        point.latitude - latDiff,
        point.longitude - lngDiff,
        point.latitude + latDiff,
        point.longitude + lngDiff);
    var visitedHouseView =
        Provider.of<AbstractVisitedHousesService>(context, listen: false)
            .getVisitedHousesInArea(bbox);
    var newHouseFromServer = await showAddVisitationDialog(
        context: context,
        visitedHouseView: visitedHouseView,
        currentZoomFactor: widget.mapController.zoom);
    if (newHouseFromServer == null) return;

    var arguments =
        await showArgumentsDialog(context: context, coordinates: point);
    if (arguments != null)
      Provider.of<AbstractArgumentsService>(context, listen: false)
          .createArguments(arguments);

    setState(() {
      this.housePolygons = generateVisitedHousePolygons();
    });
  }

  update(List<int> initiativeIds) async {
    placardService.loadPlacards(initiativeIds).then((placards) => setState(() {
          this.placards = placards;
        }));
    Provider.of<AbstractVisitedHousesService>(context, listen: false)
        .loadVisitedHouses()
        .then((_) => setState(
            () => this.housePolygons = this.generateVisitedHousePolygons()));
  }

  List<ActionMarker> generateActionMarkers() {
    return widget.termine
        .map((action) => ActionMarker(action,
            ownAction: widget.isMyAction(action),
            participant: widget.iAmParticipant(action),
            onTap: widget.openActionDetails))
        .toList()
        .reversed
        .toList();
  }

  List<Marker> generateListLocationMarkers() {
    if (!initialized) return [];
    return widget.listLocations
        .map((listlocation) =>
            ListLocationMarker(listlocation, onTap: openListLocationDialog))
        .toList();
  }

  List<PlacardMarker> generatePlacardMarkers() {
    if (!initialized || widget.mapController.zoom < placardMinZoom) return [];
    return placards
        .map((placard) => PlacardMarker(placard,
            mine: placard.benutzer == widget.myUserId,
            onTap: openPlacardDialog))
        .toList()
        .reversed
        .toList();
  }

  Color generateColor(String bezirk) {
    return Color.fromARGB(150, bezirk.hashCode * 10, bezirk.hashCode * 100,
        bezirk.hashCode * 1000);
  }
}

class ActionMarker extends Marker {
  bool ownAction = false;
  Function(Termin) onTap;
  bool participant = false;

  static emptyFunction(_) {}

  ActionMarker(Termin action,
      {this.ownAction = false,
      this.onTap = emptyFunction,
      this.participant = false})
      : super(
          width: 30.0,
          height: 30.0,
          point: LatLng(action.latitude, action.longitude),
          builder: (context) => DecoratedBox(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(offset: Offset(-2.0, 2.0), blurRadius: 4.0)
              ], shape: BoxShape.circle),
              child: TextButton(
                  key: Key('action marker'),
                  onPressed: () => onTap(action),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        CampaignTheme.actionColor(
                            action, ownAction, participant)),
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                        CircleBorder(
                            side: BorderSide(
                                color: CampaignTheme.black, width: 1.0))),
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(0)),
                  ),
                  child: Image.asset(action.getAsset(centered: true),
                      alignment: Alignment.center))),
        );
}

class PlacardMarker extends Marker {
  bool mine = false;
  Function(Placard, bool) onTap;

  static emptyFunction(_1, _2) {}

  PlacardMarker(Placard placard,
      {this.mine = false, this.onTap = emptyFunction})
      : super(
          width: 30.0,
          height: 30.0,
          point: LatLng(placard.latitude, placard.longitude),
          builder: (context) => DecoratedBox(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                    offset: Offset(0.0, 0.0),
                    blurRadius: 6,
                    spreadRadius: 3,
                    color: CampaignTheme.placardColor(mine, placard.initiative))
              ], shape: BoxShape.circle),
              child: TextButton(
                  key: Key('placard marker'),
                  onPressed:
                      placard.abgehangen ? null : () => onTap(placard, mine),
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                        EdgeInsets.all(0)),
                  ),
                  child: placard.abgehangen
                      ? Image.asset('assets/images/PlakatAbgehangen.png')
                      : Image.asset('assets/images/Plakat.png'))),
        );
}

class ListLocationMarker extends Marker {
  Function(ListLocation) onTap;
  static emptyFunction(_1) {}

  ListLocationMarker(ListLocation listLocation, {this.onTap = emptyFunction})
      : super(
            width: 25.0,
            height: 25.0,
            anchorPos: AnchorPos.align(AnchorAlign.top),
            point: LatLng(listLocation.latitude, listLocation.longitude),
            builder: (context) => TextButton(
                key: Key('list location marker'),
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                      EdgeInsets.all(0)),
                ),
                onPressed: () => onTap(listLocation),
                child: Image.asset('assets/images/Soliort_black.png')));
}
