import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quiver/strings.dart';
import 'package:sammel_app/model/Evaluation.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/model/Termin.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';

enum ValidationState { not_validated, error, ok, pressed, unpressed }

const ratingOptions = {
  'sehr cool': 5,
  'gut': 4,
  'ganz okay': 3,
  'mäßig': 2,
  'doof': 1
};

const locationTypes = [
  'Park',
  'Belebter Platz/Straße',
  'Vor einem Event',
  'Vor einem Geschäft/Einkaufszentrum',
  'Andere'
];

class EvaluationData {
  bool fuerAlle = false;
  int? teilnehmer;
  int? unterschriften;
  bool? schaetzung = false;
  String? bewertung;
  double? stunden;
  String? verbesserung = '';
  String? beschreibung = '';
  String? sonstiges = '';
  bool ausgefallen = false;
  bool inBewegung = false;
  List<String> ortTypen = [];

  EvaluationData();

  Map<String, ValidationState> validated = {
    'teilnehmer': ValidationState.ok,
    'unterschriften': ValidationState.not_validated,
    'bewertung': ValidationState.not_validated,
    'ort_typen': ValidationState.not_validated,
    'stunden': ValidationState.ok,
    'schaetzung': ValidationState.ok,
    'in_bewegung': ValidationState.ok,
    'fuer_alle': ValidationState.ok,
    'verbesserung': ValidationState.ok,
    'beschreibung': ValidationState.ok,
    'sonstiges': ValidationState.ok,
    'finish_pressed': ValidationState.unpressed,
  };
}

// ignore: must_be_immutable
class EvaluationForm extends StatefulWidget {
  Function(Evaluation, Termin) onFinish;
  final Termin action;

  static emptyFunction(_, __) {}

  EvaluationForm(this.action, {this.onFinish = emptyFunction, Key? key})
      : super(key: key);

  @override
  EvaluationFormState createState() => EvaluationFormState(this.action);
}

class EvaluationFormState extends State<EvaluationForm> {
  EvaluationData evaluation = EvaluationData();
  List<Initiative> inisWithRegisterSignaturesUrl = [];
  List<String> selectedLocationTypes = [];
  String? otherLocationType;

  EvaluationFormState(Termin action) : super() {
    evaluation.teilnehmer = action.participants?.length;
    evaluation.stunden =
        action.ende?.difference(action.beginn).inHours.toDouble();
    inisWithRegisterSignaturesUrl = action.initiatives
        .where((ini) => ini.registerSignaturesUrl != null)
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        body: Container(
          decoration: BoxDecoration(color: CampaignTheme.white),
          child: ListView(
              padding: EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 15.0, bottom: 50.0),
              children: <Widget>[
                Text(
                  'Dein Feedback zur Sammlung!',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ).tr(),
                SizedBox(height: 15),
                motivationText(),
                Text('Du hast in der Gruppe gesammelt und weißt nicht mehr, wer welche Unterschriften gesammelt hat? Dann kannst du auch die Unterschriften für die ganze Gruppe eintragen:')
                    .tr(),
                SwitchListTile(
                    controlAffinity: ListTileControlAffinity.leading,
                    contentPadding: EdgeInsets.zero,
                    visualDensity:
                        VisualDensity(horizontal: -4.0, vertical: -4.0),
                    title: Text("Ich trage für alle ein").tr(),
                    value: evaluation.fuerAlle,
                    onChanged: (bool value) {
                      setState(() {
                        evaluation.fuerAlle = value;
                      });
                    }),
                SizedBox(height: 15),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wie viele Unterschriften ${evaluation.fuerAlle ? 'habt IHR' : 'hast DU'} gesammelt?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            onTap: unterschriftenSelection,
                            child: unterschriftenButtonCaption(this.evaluation))
                      ]))
                ]),
                CheckboxListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  contentPadding: EdgeInsets.zero,
                  visualDensity:
                      VisualDensity(horizontal: -4.0, vertical: -4.0),
                  title: Text("Das ist eine Schätzung",
                      style: TextStyle(
                        fontSize: 15.0,
                      )).tr(),
                  value: evaluation.schaetzung,
                  onChanged: (bool? value) {
                    setState(() {
                      evaluation.schaetzung = value;
                    });
                  },
                ),
                SizedBox(height: 15),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wie viele Stunden ${evaluation.fuerAlle ? 'wart IHR in Summe' : 'warst DU'} sammeln?',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ).tr(),
                        InputButton(
                            onTap: stundenSelection,
                            child: stundenButtonCaption(this.evaluation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wie viele Menschen haben gesammelt?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            onTap: teilnehmerSelection,
                            child: teilnehmerButtonCaption(this.evaluation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wie war der Spaßfaktor bei der Sammlung?',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ).tr(),
                        InputButton(
                            onTap: bewertungSelection,
                            child: bewertungButtonCaption(this.evaluation)),
                      ]))
                ]),
                SizedBox(height: 15),
                Text(
                  'Feedback zum Ort',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ).tr(),
                SizedBox(height: 15),
                Text(
                    'Bitte beantworte noch ein paar Fragen zu dem Ort, an dem du gesammelt hast.'),
                SizedBox(height: 15),
                Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Flexible(
                        child: Container(
                            child: Column(
                          children: [
                            Text(
                              'In welche der folgenden Kategorien passt der Ort, an dem du gesammelt hast?',
                              style: CampaignTheme.headingStyle,
                            ).tr(),
                            if (evaluation.validated['ort_typen'] ==
                                ValidationState.error)
                              Row(children: [
                                Text("Gib bitte mindestens eine Kategorie an")
                                    .tr(),
                                Icon(Icons.error_outline)
                              ]),
                            for (String locationType in locationTypes)
                              CheckboxListTile(
                                  controlAffinity:
                                      ListTileControlAffinity.leading,
                                  contentPadding: EdgeInsets.zero,
                                  visualDensity: VisualDensity(
                                      horizontal: -4.0, vertical: -2.0),
                                  value: selectedLocationTypes
                                      .contains(locationType),
                                  onChanged: (value) => setState(() {
                                        if (value == true) {
                                          selectedLocationTypes
                                              .add(locationType);
                                        } else {
                                          selectedLocationTypes
                                              .remove(locationType);
                                        }
                                      }),
                                  title: Text(locationType).tr()),
                            if (selectedLocationTypes.contains('Andere'))
                              TextField(
                                key: Key('other location type input'),
                                decoration: InputDecoration(
                                    hintText:
                                        'Bitte gib eine Beschreibung ein...'
                                            .tr()),
                                maxLength: 50,
                                onChanged: (input) =>
                                    setState(() => otherLocationType = input),
                              ),
                          ],
                        )),
                      )
                    ]),
                SizedBox(height: 15),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Warst du bei der Sammlung immer am selben Ort oder in Bewegung?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        SwitchListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            contentPadding: EdgeInsets.zero,
                            visualDensity:
                                VisualDensity(horizontal: -4.0, vertical: -4.0),
                            title: Text(
                                    "Ich war in Bewegung (300 Meter oder mehr)")
                                .tr(),
                            value: evaluation.inBewegung,
                            onChanged: (bool value) {
                              setState(() {
                                evaluation.inBewegung = value;
                              });
                            }),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Bitte beschreibe den Ort / die Orte kurz:',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ).tr(),
                        InputButton(
                            onTap: beschreibungSelection,
                            child: beschreibungButtonCaption(this.evaluation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Hat dir noch etwas gefehlt, um besser sammeln zu können?',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ).tr(),
                        InputButton(
                            onTap: verbesserungSelection,
                            child: verbesserungButtonCaption(this.evaluation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Hast du sonst noch eine Rückmeldung für uns?',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ).tr(),
                        InputButton(
                            onTap: sonstigesSelection,
                            child: sonstigesButtonCaption(this.evaluation)),
                      ]))
                ]),
                SizedBox(
                  height: 30,
                )
              ]),
        ),
        bottomSheet: Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(blurRadius: 5.0, color: Colors.black38)],
              color: CampaignTheme.lightgray),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            TextButton(
                key: Key('evaluation editor cancel button'),
                style: CampaignTheme.cancelButtonStyle,
                child: Text('Abbrechen').tr(),
                onPressed: () {
                  setState(() => evaluation = EvaluationData());
                  Navigator.maybePop(context);
                }),
            TextButton(
                key: Key('evaluation editor finish button'),
                style: CampaignTheme.yesButtonStyle,
                child: Text('Fertig').tr(),
                onPressed: () => finishPressed())
          ]),
        ));
  }

  static Widget motivationText() {
    return Column(key: Key('motivation text'), children: [
      Text(
        'Danke, dass du gesammelt hast! Lass uns als erstes schauen, wie viele Unterschriften zusammengekommen sind.\n',
        textScaleFactor: 1.0,
      ).tr()
    ]);
  }

  Future<String?> showTextInputDialog(
      String? initialValue, String title, String? description, Key key) {
    String? currentValue = initialValue;
    TextFormField inputField = TextFormField(
      minLines: 3,
      initialValue: initialValue,
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
      ),
      onChanged: (newValue) => currentValue = newValue,
    );

    Widget inputWidget;

    if (description != null) {
      inputWidget = SingleChildScrollView(
          child: ListBody(children: [
        Text(description).tr(),
        SizedBox(height: 10),
        inputField
      ]));
    } else {
      inputWidget = inputField;
    }

    Widget cancelButton = TextButton(
      style: CampaignTheme.cancelButtonStyle,
      child: Text("Abbrechen").tr(),
      onPressed: () {
        Navigator.pop(context, initialValue);
      },
    );
    Widget continueButton = TextButton(
      style: CampaignTheme.yesButtonStyle,
      child: Text("Fertig").tr(),
      onPressed: () {
        Navigator.pop(context, currentValue);
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      key: key,
      title: Text(title).tr(),
      content: inputWidget,
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<String?> showRadioInputDialog(
      String? init, String title, String? description, Key key) {
    String? value = init;

    // show the dialog
    return showDialog(
        context: context,
        builder: (context) => StatefulBuilder(
              builder: (context, setDialogState) {
                Widget input = Column(
                    children: ratingOptions.keys
                        .map((option) => RadioListTile(
                              title: Text(option).tr(),
                              value: option,
                              groupValue: value,
                              onChanged: (String? selection) =>
                                  setDialogState(() => value = selection),
                            ))
                        .toList());

                if (description != null)
                  input = SingleChildScrollView(
                      child: ListBody(children: [
                    Text(description).tr(),
                    SizedBox(height: 10),
                    input
                  ]));

                return AlertDialog(
                  key: key,
                  title: Text(title).tr(),
                  content: input,
                  actions: [
                    TextButton(
                      style: CampaignTheme.cancelButtonStyle,
                      child: Text("Abbrechen").tr(),
                      onPressed: () => Navigator.pop(context, init),
                    ),
                    TextButton(
                      style: CampaignTheme.yesButtonStyle,
                      child: Text("Fertig").tr(),
                      onPressed: () => Navigator.pop(context, value),
                    ),
                  ],
                );
              },
            ));
  }

  void teilnehmerSelection() async {
    var ergebnis = await showIntegerInputDialog(
        context,
        // should be number input
        this.evaluation.teilnehmer,
        'Anzahl Teilnehmer*innen',
        'Wie viele Leute waren bei der Aktion dabei?',
        Key('teilnehmer input dialog'));
    setState(() {
      this.evaluation.teilnehmer = ergebnis ?? this.evaluation.teilnehmer;
      validateAllInput();
    });
  }

  Widget teilnehmerButtonCaption(EvaluationData evaluation) {
    Text text;
    if (this.evaluation.validated['teilnehmer'] == ValidationState.ok) {
      text = Text('{} Teilnehmer').plural(evaluation.teilnehmer ?? 0);
    } else {
      text = Text('Wie viele Leute haben teilgenommen?',
              style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.evaluation.validated['teilnehmer']!);
  }

  void unterschriftenSelection() async {
    var ergebnis = await showIntegerInputDialog(
        context,
        this.evaluation.unterschriften,
        'Anzahl Deiner Unterschriften',
        evaluation.fuerAlle
            ? 'Wie viele Unterschriften habt IHR gesammelt?'
            : 'Wie viele Unterschriften hast DU persönlich gesammelt?',
        Key('unterschriften input dialog'));
    setState(() {
      this.evaluation.unterschriften =
          ergebnis ?? this.evaluation.unterschriften;
      validateAllInput();
    });
  }

  Widget unterschriftenButtonCaption(EvaluationData evaluation) {
    Text text;
    if (this.evaluation.validated['unterschriften'] == ValidationState.ok) {
      text = Text('{} Unterschriften').plural(evaluation.unterschriften!);
    } else {
      text = Text('Gib hier die Unterschriftenanzahl an',
              style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.evaluation.validated['unterschriften']!);
  }

  void bewertungSelection() async {
    var ergebnis = await showRadioInputDialog(
        this.evaluation.bewertung,
        'Spaßfaktor',
        'Wie fandest Du die Aktion?',
        Key('bewertung input dialog'));
    setState(() {
      this.evaluation.bewertung = ergebnis;
      validateAllInput();
    });
  }

  Widget bewertungButtonCaption(EvaluationData evaluation) {
    Text text;
    if (this.evaluation.validated['bewertung'] == ValidationState.ok) {
      text = Text(evaluation.bewertung!).tr();
    } else {
      text =
          Text('Wie fandest Du die Aktion?', style: CampaignTheme.detailStyle)
              .tr();
    }
    return buildTextRow(text, this.evaluation.validated['bewertung']!);
  }

  void stundenSelection() async {
    var ergebnis = await showIntegerInputDialog(
        context,
        // should be number input
        this.evaluation.stunden?.round(),
        evaluation.fuerAlle
            ? 'Wie viele Stunden wart IHR in Summe sammeln? Beispiel: Wenn ihr zu dritt 2 Stunden sammeln wart, dann sind das 6 Stunden.'
            : 'Wie viele Stunden warst DU sammeln?',
        'Auf die nächste Stunde gerundet',
        Key('stunden input dialog'));
    setState(() {
      this.evaluation.stunden = ergebnis?.toDouble() ?? this.evaluation.stunden;
      validateAllInput();
    });
  }

  Widget stundenButtonCaption(EvaluationData evaluation) {
    Text text;
    if (this.evaluation.validated['stunden'] == ValidationState.ok) {
      text = Text('{} Stunden').plural(evaluation.stunden ?? 0);
    } else {
      text = Text('Wie viele Stunden habt ihr gesammelt?',
              style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.evaluation.validated['stunden']!);
  }

  void verbesserungSelection() async {
    var ergebnis = await showTextInputDialog(
        this.evaluation.verbesserung,
        'Hat was gefehlt?',
        'Optional: Hat dir noch etwas gefehlt, um besser sammeln zu können?',
        Key('improvement input dialog'));
    setState(() {
      this.evaluation.verbesserung = ergebnis ?? this.evaluation.verbesserung;
      validateAllInput();
    });
  }

  Widget verbesserungButtonCaption(EvaluationData evaluation) {
    Text text;
    if (!isBlank(this.evaluation.verbesserung)) {
      text = Text(evaluation.verbesserung!);
    } else {
      text =
          Text('Optional: Deine Gedanken...', style: CampaignTheme.detailStyle)
              .tr();
    }
    return buildTextRow(text, this.evaluation.validated['verbesserung']!);
  }

  void beschreibungSelection() async {
    var ergebnis = await showTextInputDialog(
        this.evaluation.beschreibung,
        'Beschreibung des Ortes',
        'Optional: Wie war die Situation vor Ort? (Wetter, Veranstaltung in der Nähe, besonderer Anlass, ...)',
        Key('description input dialog'));
    setState(() {
      this.evaluation.beschreibung = ergebnis ?? this.evaluation.beschreibung;
      validateAllInput();
    });
  }

  Widget beschreibungButtonCaption(EvaluationData evaluation) {
    Text text;
    if (!isBlank(this.evaluation.beschreibung)) {
      text = Text(evaluation.beschreibung!);
    } else {
      text = Text('Optional: Beschreibung...', style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.evaluation.validated['beschreibung']!);
  }

  void sonstigesSelection() async {
    var ergebnis = await showTextInputDialog(
        this.evaluation.sonstiges,
        'Sonstige Rückmeldung',
        'Optional: Willst du uns noch etwas mitteilen?',
        Key('description input dialog'));
    setState(() {
      this.evaluation.sonstiges = ergebnis ?? this.evaluation.sonstiges;
      validateAllInput();
    });
  }

  Widget sonstigesButtonCaption(EvaluationData evaluation) {
    Text text;
    if (!isBlank(this.evaluation.sonstiges)) {
      text = Text(evaluation.sonstiges!);
    } else {
      text = Text('Optional: Weitere Rückmeldung...',
              style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.evaluation.validated['beschreibung']!);
  }

  void validateAllInput() {
    validateInt(evaluation.unterschriften, 'unterschriften');
    validateString(evaluation.bewertung, 'bewertung');
    validateDouble(evaluation.stunden, 'stunden');
    validateLocationTypes(evaluation.ortTypen);

    evaluation.validated['all'] = ValidationState.ok;
    for (var value in evaluation.validated.values) {
      if (value == ValidationState.error ||
          value == ValidationState.not_validated) {
        evaluation.validated['all'] = ValidationState.error;
        break;
      }
    }
  }

  void validateLocationTypes(List<String> locationTypes) {
    if (locationTypes.isEmpty) {
      this.evaluation.validated['ort_typen'] = ValidationState.error;
    } else {
      this.evaluation.validated['ort_typen'] = ValidationState.ok;
    }
  }

  void validateDouble(double? field, name) {
    this.evaluation.validated[name] = (field != null && field > 0)
        ? ValidationState.ok
        : ValidationState.error;
  }

  void validateString(String? field, name) {
    this.evaluation.validated[name] = field != null && field.isNotEmpty
        ? ValidationState.ok
        : ValidationState.error;
  }

  void validateInt(int? field, name) {
    this.evaluation.validated[name] =
        field != null ? ValidationState.ok : ValidationState.error;
  }

  finishPressed() async {
    setState(() {
      // Compute array of location types depending on select location types
      evaluation.ortTypen = [];

      for (var type in selectedLocationTypes) {
        if (type != 'Andere') {
          evaluation.ortTypen.add(type);
        } else if (otherLocationType != null && otherLocationType!.isNotEmpty) {
          evaluation.ortTypen.add(otherLocationType!);
        }
      }

      evaluation.validated['finish_pressed'] = ValidationState.pressed;
      validateAllInput();
    });
    if (evaluation.validated['all'] == ValidationState.ok) {
      validateAllInput();
      if (evaluation.validated['all'] == ValidationState.ok) {
        widget.onFinish(
            Evaluation(
                widget.action.id,
                evaluation.teilnehmer,
                evaluation.fuerAlle,
                evaluation.schaetzung,
                evaluation.unterschriften,
                evaluation.stunden,
                evaluation.ortTypen,
                evaluation.inBewegung,
                ratingOptions[evaluation.bewertung],
                evaluation.beschreibung,
                evaluation.verbesserung,
                evaluation.sonstiges),
            widget
                .action); // maybe the Evaluation/EvaluationData two-tap is superfluous here
        setState(
            () => evaluation = EvaluationData()); // reset Form for next use
      }
    }
  }

  Row buildTextRow(Text text, ValidationState valState) {
    List<Widget> w = [Flexible(child: text)];
    if (evaluation.validated['finish_pressed'] == ValidationState.pressed) {
      if (valState == ValidationState.ok)
        w.add(Icon(Icons.done, color: Colors.black));
      else
        w.add(Icon(Icons.error_outline));
    }
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: w);
  }
}

class InputButton extends StatelessWidget {
  final Function() onTap;
  final Widget child;

  InputButton({required this.onTap, required this.child, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.only(bottom: 8.0, top: 4.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                      child: Container(
                    child: child,
                    color: CampaignTheme.darkorange,
                    padding: EdgeInsets.all(5.0),
                  ))
                ])),
        onTap: onTap);
  }
}

Future<int?> showIntegerInputDialog(BuildContext context, int? init,
    String title, String? description, Key key) {
  var controller = TextEditingController(text: init?.toString() ?? '');
  Widget content = Row(mainAxisAlignment: MainAxisAlignment.center, children: [
    Flexible(
        child: TextButton(
            child: Icon(Icons.remove_circle_outline),
            onPressed: () {
              final number = int.tryParse(controller.text);
              if (number == null) return;
              if (number < 1)
                return;
              else
                controller.text = (number - 1).toString();
            })),
    Flexible(
        child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 100.0, minWidth: 50.0),
            child: TextFormField(
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              textAlign: TextAlign.center,
              controller: controller,
              onTap: () => controller.selection = TextSelection(
                  baseOffset: 0, extentOffset: controller.text.length),
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
              ),
              // onChanged: (newValue) => controller.text = newValue,
            ))),
    Flexible(
        child: TextButton(
            child: Icon(Icons.add_circle_outline),
            onPressed: () {
              final number = int.tryParse(controller.text);
              if (number == null)
                controller.text = '1';
              else
                controller.text = (number + 1).toString();
            })),
  ]);

  if (description != null) {
    content = SingleChildScrollView(
        child: ListBody(
            children: [Text(description).tr(), SizedBox(height: 10), content]));
  }

  // show the dialog
  return showDialog(
      context: context,
      builder: (context) => AlertDialog(
            key: key,
            title: Text(title).tr(),
            content: content,
            actions: [
              TextButton(
                style: CampaignTheme.cancelButtonStyle,
                child: Text("Abbrechen").tr(),
                onPressed: () {
                  Navigator.pop(context, init);
                },
              ),
              TextButton(
                style: CampaignTheme.yesButtonStyle,
                child: Text("Fertig").tr(),
                onPressed: () {
                  Navigator.pop(context, int.tryParse(controller.text));
                },
              ),
            ],
          ));
}
