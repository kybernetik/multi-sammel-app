import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';

Future<Initiative> showInitiativePickerDialog(BuildContext context) async {
  List<Initiative> allInitiatives =
      await Provider.of<AbstractInitiativesService>(context, listen: false)
          .loadInitiatives();

  return await showDialog(
      context: context,
      builder: (_) => AlertDialog(
              key: Key('initiative picker dialog'),
              title: Text('Initiativenauswahl').tr(),
              content: SingleChildScrollView(
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text('Für welche Initiative?').tr(),
                SizedBox(height: 10),
                for (Initiative ini in allInitiatives)
                  TextButton(
                      key: Key('initiative picker button ${ini.id}'),
                      child: Container(
                          alignment: Alignment.center,
                          child: Row(
                              children: [Expanded(child: Text(ini.longName))]),
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                              border: Border.all(color: CampaignTheme.black),
                              borderRadius: BorderRadius.circular(10.0),
                              color: CampaignTheme.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.grey,
                                    offset: Offset(1.0, 1.0),
                                    blurRadius: 3.0,
                                    spreadRadius: 1.0)
                              ])),
                      onPressed: () => Navigator.pop(context, ini)),
              ])),
              actions: <Widget>[
                TextButton(
                  key: Key('map action dialog abort button'),
                  style: CampaignTheme.cancelButtonStyle,
                  child: Text('Abbrechen').tr(),
                  onPressed: () => Navigator.pop(context),
                )
              ]));
}
