import 'package:after_layout/after_layout.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/model/Kiez.dart';
import 'package:sammel_app/model/ListLocation.dart';
import 'package:sammel_app/services/ErrorService.dart';
import 'package:sammel_app/services/GeoService.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/StammdatenService.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';
import 'package:provider/provider.dart';

import 'LocationDialog.dart';

enum ValidationState { not_validated, error, ok, pressed, unpressed }

class ListLocationData {
  String? street;
  String? number;
  String? name;
  String? description;
  LatLng? coordinates;
  String? contact;
  late List<Initiative> initiatives;

  ListLocationData(
      [this.street,
      this.number,
      this.name,
      this.description,
      this.coordinates,
      this.contact,
      List<Initiative>? initiatives]) {
    this.initiatives = initiatives ?? [];
  }

  Map<String, ValidationState> validated = {
    'street': ValidationState.ok,
    'nr': ValidationState.ok,
    'name': ValidationState.not_validated,
    'description': ValidationState.ok,
    'contact': ValidationState.not_validated,
    'venue': ValidationState.not_validated,
    'initiatives': ValidationState.not_validated,
    'all': ValidationState.not_validated,
    'finish_pressed': ValidationState.unpressed,
  };
}

// ignore: must_be_immutable
class ListLocationEditor extends StatefulWidget {
  final ListLocation? initListLocation;
  late Function onFinish;

  ListLocationEditor({this.initListLocation, required this.onFinish, Key? key})
      : super(key: key);

  @override
  ListLocationEditorState createState() =>
      ListLocationEditorState(this.initListLocation);
}

class ListLocationEditorState extends State<ListLocationEditor>
    with AfterLayoutMixin<ListLocationEditor> {
  ListLocationData listLocation = ListLocationData();
  List<int>? initInitiativeIds = null;

  ListLocationEditorState(ListLocation? initListLocation) : super() {
    if (initListLocation != null) assignInitialListLocation(initListLocation);
    validateAllInput();
  }

  void assignInitialListLocation(ListLocation initListLocation) {
    listLocation.street = initListLocation.street;
    listLocation.number = initListLocation.number;
    listLocation.name = initListLocation.name;
    listLocation.coordinates =
        LatLng(initListLocation.latitude, initListLocation.longitude);
    listLocation.initiatives = initListLocation.initiatives;
    listLocation.contact = initListLocation.contact;
    listLocation.description = initListLocation.description;

    // For some reason initInitiatives changes when selecting a new initiative
    // (probably makes sense in the dart world, I just don't get it yet),
    // therefore we want to clone the array
    initInitiativeIds =
        initListLocation.initiatives.map((ini) => ini.id).toList();
  }

  get isNewListLocation => widget.initListLocation == null;

  @override
  void afterFirstLayout(BuildContext context) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        body: Container(
          decoration: BoxDecoration(color: CampaignTheme.white),
          child: ListView(
              padding: EdgeInsets.only(
                  left: 20.0, right: 20.0, top: 15.0, bottom: 50.0),
              children: <Widget>[
                isNewListLocation ? motivationText() : Container(),
                SizedBox(height: 15),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Icon(Icons.my_location, size: 40.0),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wo?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            key: Key('Open location dialog'),
                            onTap: locationSelection,
                            child: locationButtonCaption(this.listLocation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Icon(Icons.emoji_flags, size: 40.0),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wofür?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            key: Key('action editor initiatives button'),
                            onTap: () => initiativesSelection(),
                            child: initiativesButtonCaption(this.listLocation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Icon(Icons.info_outline, size: 40.0),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Weiteres?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            onTap: descriptionSelection,
                            child: descriptionButtonCaption(this.listLocation)),
                      ]))
                ]),
                Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Icon(Icons.face, size: 40.0),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                        Text(
                          'Wer?',
                          style: CampaignTheme.headingStyle,
                        ).tr(),
                        InputButton(
                            key: Key('action editor contact button'),
                            onTap: contactSelection,
                            child: contactButtonCaption(this.listLocation)),
                      ]))
                ]),
              ]),
        ),
        bottomSheet: Container(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(blurRadius: 5.0, color: Colors.black38)],
              color: CampaignTheme.lightgray),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            TextButton(
                key: Key('action editor cancel button'),
                style: CampaignTheme.cancelButtonStyle,
                child: Text('Abbrechen').tr(),
                onPressed: () {
                  resetListLocation();
                  Navigator.maybePop(context);
                }),
            TextButton(
                key: Key('list location editor finish button'),
                style: CampaignTheme.yesButtonStyle,
                child: Text('Fertig').tr(),
                onPressed: () => finishPressed())
          ]),
        ));
  }

  static Widget motivationText() =>
      Column(key: Key('motivation text'), children: [
        Text(
          'Das Volksbegehren lebt von deiner Beteiligung! \n',
          style: CampaignTheme.headingStyle,
        ).tr(),
        Text(
          'Wenn du Listen an einem Ort ausgelegt hast, kannst du ihn hier eintragen. '
          'Die Inhaber*innen des Ortes müssen die Zustimmung erteilt haben, dass der Ort hier öffentlich angezeigt wird.',
          textScaleFactor: 1.0,
        ).tr()
      ]);

  void locationSelection({LatLng? initPosition}) async {
    Location? result = await showLocationDialog(
        context: context,
        initDescription: listLocation.name,
        initCoordinates: listLocation.coordinates ?? initPosition,
        // initKiez: null, // TODO?
        initStreet: listLocation.street,
        initNumber: listLocation.number,
        center: determineMapCenter(listLocation),
        editor: 'listLocation');

    setNewLocation(result);
  }

  void setNewLocation(Location? result) {
    setState(() {
      listLocation.name = result?.description;
      listLocation.coordinates = result?.coordinates;
      listLocation.street = result?.street;
      listLocation.number = result?.number;
      validateAllInput();
    });
  }

  static LatLng? determineMapCenter(ListLocationData listLocation) {
    // at old coordinates
    if (listLocation.coordinates?.latitude == null &&
        listLocation.coordinates?.longitude == null)
      return null;
    else
      return LatLng(listLocation.coordinates!.latitude,
          listLocation.coordinates!.longitude);
  }

  void contactSelection() async {
    var ergebnis = await showTextInputDialog(
        this.listLocation.contact,
        'Kontakt',
        'Gib ein paar Infos über dich als Ansprechpartner*in an. Wie kann man dich kontaktieren?\nBeachte, dass alle Angaben in der App zu sehen sein werden!',
        Key('contact input dialog'));
    setState(() {
      this.listLocation.contact = ergebnis;
      validateAllInput();
    });
  }

  Future<String?> showTextInputDialog(
      String? initialValue, String title, String? description, Key key) {
    String? currentValue = initialValue;

    TextFormField inputField = TextFormField(
      key: Key('text input dialog field'),
      minLines: 3,
      initialValue: initialValue,
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
      ),
      onChanged: (newValue) {
        currentValue = newValue;
      },
    );

    return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        key: key,
        title: Text(title).tr(),
        content: description != null
            ? SingleChildScrollView(
                child: ListBody(children: [
                Text(description).tr(),
                SizedBox(height: 10),
                inputField
              ]))
            : inputField,
        actions: [
          TextButton(
            child: Text("Abbrechen").tr(),
            onPressed: () {
              Navigator.pop(context, initialValue);
            },
          ),
          TextButton(
            key: Key('list location editor text input accept button'),
            child: Text("Fertig").tr(),
            onPressed: () => Navigator.pop(context, currentValue),
          ),
        ],
      ),
    );
  }

  initiativesSelection() async {
    List<Initiative> selectedInitiatives = listLocation.initiatives;
    List<Initiative> allInitiatives =
        await Provider.of<AbstractInitiativesService>(context, listen: false)
            .loadInitiatives();

    await showDialog<String>(
        context: context,
        builder: (context) =>
            StatefulBuilder(builder: (context, setDialogState) {
              print('selected ${selectedInitiatives.length}');

              return SimpleDialog(
                  key: Key('initiatives selection dialog'),
                  contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                  titlePadding: EdgeInsets.all(15.0),
                  title: const Text('Wähle Initiativen').tr(),
                  children: []
                    ..addAll(allInitiatives.map((ini) => CheckboxListTile(
                          value: selectedInitiatives.contains(ini),
                          title: Text(ini.longName),
                          // If the list location is edited (initListLocation exists) we want the user to not be able
                          // to remove the last initiative. Therefore we pass null to onChanged
                          onChanged: selectedInitiatives.contains(ini) &&
                                  selectedInitiatives.length == 1
                              ? null
                              : (bool? checked) {
                                  setDialogState(() {
                                    if (checked == null || checked == false) {
                                      selectedInitiatives.remove(ini);
                                    } else {
                                      selectedInitiatives.add(ini);
                                    }
                                  });
                                },
                        )))
                    ..add(TextButton(
                        child: Text('Fertig').tr(),
                        style: CampaignTheme.yesButtonStyle,
                        onPressed: () => Navigator.pop(context))));
            }));

    setState(() {
      this.listLocation.initiatives = selectedInitiatives;
      validateAllInput();
    });
  }

  void descriptionSelection() async {
    var ergebnis = await showTextInputDialog(
        this.listLocation.description,
        'Beschreibung',
        'Gib optional eine Beschreibung des Ortes oder zusätzliche Infos an. Wann wurden zuletzt Listen abgeholt?',
        Key('description input dialog'));
    setState(() {
      this.listLocation.description = ergebnis;
      validateAllInput();
    });
  }

  Widget locationButtonCaption(ListLocationData listLocation) {
    Text text;
    if (this.listLocation.validated['venue'] == ValidationState.error ||
        this.listLocation.validated['venue'] == ValidationState.not_validated) {
      text = Text(
        'Gib einen Ort an',
        style: CampaignTheme.detailStyle,
      ).tr();
    } else {
      String message = 'Name: {name}';
      Map<String, String> args = {
        'name': listLocation.name!,
      };

      if (listLocation.street != null) {
        message += ', Adresse: {street}';
        args['street'] = listLocation.street!;

        if (listLocation.number != null) {
          message += ' {number}';
          args['number'] = listLocation.number!;
        }
      }
      text = Text(message).tr(namedArgs: args);
    }
    return buildTextRow(text, this.listLocation.validated['venue']!);
  }

  Widget contactButtonCaption(ListLocationData listLocation) {
    Text text;
    ValidationState val;
    if (this.listLocation.contact != null) {
      text = Text(listLocation.contact!);
      val = ValidationState.ok;
    } else {
      text = Text('Ein paar Worte über dich', style: CampaignTheme.detailStyle)
          .tr();
      val = ValidationState.error;
    }
    return buildTextRow(text, val);
  }

  Widget initiativesButtonCaption(ListLocationData listLocation) {
    Text text;
    ValidationState val;
    if (this.listLocation.validated['initiatives'] == ValidationState.ok) {
      text =
          Text(listLocation.initiatives.map((ini) => ini.longName).join(', '));
      val = ValidationState.ok;
    } else {
      text = Text('Für welche Initiative:n liegen hier Listen aus?',
              style: CampaignTheme.detailStyle)
          .tr();
      val = ValidationState.error;
    }
    return buildTextRow(text, val);
  }

  Widget descriptionButtonCaption(ListLocationData listLocation) {
    Text text;
    if (this.listLocation.description != null) {
      text = Text('Beschreibung: {beschreibung}')
          .tr(namedArgs: {'beschreibung': listLocation.description!});
    } else {
      text = Text('Gib optional Infos zum Ort an',
              style: CampaignTheme.detailStyle)
          .tr();
    }
    return buildTextRow(text, this.listLocation.validated['description']!);
  }

  void validateAllInput() {
    validateAgainstNull(listLocation.name, 'name');
    validateAgainstNull(listLocation.contact, 'contact');
    validateVenue();
    validateInitiatives();

    listLocation.validated['all'] = ValidationState.ok;
    for (var value in listLocation.validated.values) {
      if (value == ValidationState.error ||
          value == ValidationState.not_validated) {
        listLocation.validated['all'] = ValidationState.error;
        break;
      }
    }
  }

  void validateAgainstNull(field, name) {
    if (field != null) {
      this.listLocation.validated[name] = ValidationState.ok;
    } else {
      this.listLocation.validated[name] = ValidationState.error;
    }
  }

  void validateVenue() {
    if ((listLocation.name?.isEmpty ?? true) ||
        listLocation.coordinates?.latitude == null ||
        listLocation.coordinates?.longitude == null) {
      this.listLocation.validated['venue'] = ValidationState.error;
    } else
      this.listLocation.validated['venue'] = ValidationState.ok;
  }

  void validateInitiatives() {
    if (this.listLocation.initiatives.isEmpty) {
      this.listLocation.validated['initiatives'] = ValidationState.error;
    } else {
      this.listLocation.validated['initiatives'] = ValidationState.ok;
    }
  }

  Future<ListLocation?> generateListLocation() async {
    validateAllInput();
    if (listLocation.validated['all'] == ValidationState.ok) {
      return ListLocation(
          widget.initListLocation?.id,
          this.listLocation.name,
          this.listLocation.street,
          this.listLocation.number,
          this.listLocation.description,
          this.listLocation.contact,
          this.listLocation.coordinates!.latitude,
          this.listLocation.coordinates!.longitude,
          this.listLocation.initiatives);
    } else {
      return null;
    }
  }

  finishPressed() async {
    setState(() {
      listLocation.validated['finish_pressed'] = ValidationState.pressed;
      validateAllInput();
    });
    if (listLocation.validated['all'] == ValidationState.ok) {
      ListLocation? listLocation = await generateListLocation();
      if (listLocation != null) {
        widget.onFinish(listLocation);
        if (isNewListLocation)
          resetListLocation();
        else
          Navigator.maybePop(context);
      }
    }
  }

  void resetListLocation() {
    setState(() => listLocation = ListLocationData());
  }

  setPosition(LatLng position) async =>
      setNewLocation(await getDescriptionAndKiezToPoint(position));

  Future<Location> getDescriptionAndKiezToPoint(LatLng point) async {
    var geodata = await Provider.of<GeoService>(context, listen: false)
        .getDescriptionToPoint(point)
        .catchError((e, s) {
      ErrorService.handleError(e, s);
      return GeoData('', '', '');
    });

    Kiez? kiez = await Provider.of<StammdatenService>(context, listen: false)
        .getKiezAtLocation(point);

    return Location(
        geodata.description, point, kiez, geodata.street, geodata.number);
  }

  Row buildTextRow(Text text, ValidationState valState) {
    List<Widget> w = [Flexible(child: text)];
    if (listLocation.validated['finish_pressed'] == ValidationState.pressed) {
      if (valState == ValidationState.ok) {
        w.add(Icon(Icons.done, color: Colors.black));
      } else {
        w.add(Icon(Icons.error_outline));
      }
    }
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: w);
  }
}

class InputButton extends StatelessWidget {
  final Function() onTap;
  final Widget child;

  InputButton({required this.onTap, required this.child, key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.only(bottom: 8.0, top: 4.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flexible(
                      child: Container(
                    child: child,
                    color: CampaignTheme.darkorange,
                    padding: EdgeInsets.all(5.0),
                  ))
                ])),
        onTap: onTap);
  }
}
