import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:mapbox_search/mapbox_search.dart';
import 'package:provider/provider.dart';
import 'package:sammel_app/model/Kiez.dart';
import 'package:sammel_app/routes/LocationSearchWidget.dart';
import 'package:sammel_app/services/ErrorService.dart';
import 'package:sammel_app/services/GeoService.dart';
import 'package:sammel_app/services/StammdatenService.dart';
import 'package:sammel_app/shared/AttributionPlugin.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';
import 'package:sammel_app/shared/NoRotation.dart';

import '../Provisioning.dart';

Future<Location?> showLocationDialog(
        {required BuildContext context,
        String? initDescription,
        LatLng? initCoordinates,
        Kiez? initKiez,
        String? initStreet,
        String? initNumber,
        LatLng? center,
        String? editor}) =>
    showDialog(
      context: context,
      builder: (context) => LocationDialog(initDescription, initCoordinates,
          initKiez, initStreet, initNumber, center, editor),
    );

class LocationDialog extends StatefulWidget {
  final LatLng? center;
  final LatLng? initCoordinates;
  final String? initDescription;
  final String? initStreet;
  final String? initNumber;
  final Kiez? initKiez;
  final String? editor;

  LocationDialog(this.initDescription, this.initCoordinates, this.initKiez,
      this.initStreet, this.initNumber, this.center, this.editor)
      : super(key: Key('location dialog'));

  @override
  State<StatefulWidget> createState() {
    LocationMarker? initMarker =
        initCoordinates != null ? LocationMarker(initCoordinates!) : null;
    var location = Location(initDescription ?? '', initCoordinates, initKiez,
        initStreet, initNumber);
    return LocationDialogState(location, initMarker, center, editor);
  }
}

class LocationDialogState extends State<LocationDialog> {
  LocationMarker? marker;
  String? editor;
  late Location location;
  late TextEditingController venueController;
  ScrollController scrollController = ScrollController();
  bool isSearching = false;

  LocationDialogState(Location initVenue, LocationMarker? initMarker,
      LatLng? center, String? editor) {
    marker = initMarker;
    location = initVenue;
    venueController = TextEditingController(text: location.description);
    this.editor = editor;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('${editor == 'action' ? 'Treffpunkt' : 'Ort'}').tr(),
      insetPadding: EdgeInsets.all(20),
      content: SingleChildScrollView(
          controller: scrollController,
          child: ListBody(children: [
            Text(
              'Wähle auf der Karte einen ${editor == 'action' ? 'Treffpunkt' : 'Ort'} aus oder verwende die Suche.',
              textScaleFactor: 0.9,
            ).tr(),
            SizedBox(
              height: 10.0,
            ),
            LocationSearchWidget(
              onFinish: (MapBoxPlace searchedLocation) => locationSelected(
                  null,
                  LatLng(
                      searchedLocation.center![1], searchedLocation.center![0]),
                  overrideDescription: searchedLocation.placeName
                      ?.replaceFirst(', Germany', '')),
              isSearching: (value) => setState(() => isSearching = value),
            ),
            SizedBox(
              height: 10.0,
            ),
            if (!isSearching)
              ListBody(children: [
                Container(
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: CampaignTheme.black, width: 1.0)),
                    child: SizedBox(
                        height: 300.0,
                        width: 300.0,
                        child: FlutterMap(
                            key: Key('venue map'),
                            options: MapOptions(
                                center: widget.center ??
                                    LatLng(
                                        geo.initCenterLat, geo.initCenterLong),
                                zoom:
                                    widget.center != null ? geo.initZoom : 10.0,
                                interactiveFlags: noRotation,
                                swPanBoundary:
                                    LatLng(geo.boundLatMin, geo.boundLongMin),
                                nePanBoundary:
                                    LatLng(geo.boundLatMax, geo.boundLongMax),
                                maxZoom: geo.zoomMax,
                                minZoom: geo.zoomMin,
                                onTap: locationSelected,
                                plugins: [AttributionPlugin()]),
                            layers: [
                              TileLayerOptions(
                                  urlTemplate:
                                      "https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png",
                                  subdomains: ['a', 'b', 'c']),
                              MarkerLayerOptions(
                                  markers: marker == null ? [] : [marker!]),
                              AttributionOptions(),
                            ]))),
                SizedBox(
                  height: 5.0,
                ),
                Text(() {
                  if (editor == 'action' && location.kiez != null) {
                    return '${location.kiez!.name} in ${location.kiez!.region}';
                  } else if (editor == 'listLocation' &&
                      location.description != null) {
                    return location.description!;
                  }

                  return '';
                }(),
                    style: TextStyle(fontSize: 13, color: CampaignTheme.black),
                    softWrap: false,
                    overflow: TextOverflow.fade),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  editor == 'listLocation'
                      ? 'Wie ist der Name deines Soli-Ortes?'
                      : 'Du kannst eine eigene Beschreibung angeben, '
                          'z.B. "Unter der Weltzeituhr" oder "Tempelhofer Feld, '
                          'Eingang Kienitzstraße":',
                  textScaleFactor: 0.8,
                ).tr(),
                SizedBox(
                  height: 5.0,
                ),
                TextFormField(
                  key: Key('venue description input'),
                  keyboardType: TextInputType.multiline,
                  maxLength: editor == 'listLocation' ? 60 : null,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                  ),
                  onChanged: (input) => location.description = input,
                  controller: venueController,
                ),
              ])
          ])),
      actions: [
        TextButton(
          style: CampaignTheme.cancelButtonStyle,
          child: Text("Abbrechen").tr(),
          onPressed: () {
            Navigator.pop(
                context,
                Location(widget.initDescription, widget.initCoordinates,
                    widget.initKiez, widget.initStreet, widget.initNumber));
          },
        ),
        TextButton(
          style: CampaignTheme.yesButtonStyle,
          key: Key('venue dialog finish button'),
          child: Text("Fertig").tr(),
          onPressed: () {
            Navigator.pop(context, location);
          },
        ),
      ],
    );
  }

  locationSelected(var tapPosition, LatLng point,
      {String? overrideDescription}) async {
    print('location selected, $point, $overrideDescription');
    Location newLocation = await getDescriptionAndKiezToPoint(point);

    if (newLocation.kiez == null || newLocation.coordinates == null) return;

    // If description should be overriden, e.g. if location was searched...
    if (overrideDescription != null) {
      newLocation.description = overrideDescription;
    }

    scrollController.animateTo(scrollController.position.maxScrollExtent,
        duration: Duration(seconds: 1), curve: Curves.fastOutSlowIn);

    setState(() {
      location.kiez = newLocation.kiez;
      location.coordinates = point;
      venueController.text = newLocation.description!;
      location.description = newLocation.description;
      location.street = newLocation.street;
      location.number = newLocation.number;
      marker = LocationMarker(point);
    });
  }

  Future<Location> getDescriptionAndKiezToPoint(LatLng point) async {
    var geodata = await Provider.of<GeoService>(context, listen: false)
        .getDescriptionToPoint(point)
        .catchError((e, s) {
      ErrorService.handleError(e, s);
      return GeoData('', '', '');
    });

    Kiez? kiez = await Provider.of<StammdatenService>(context, listen: false)
        .getKiezAtLocation(point);

    return Location(
        geodata.description, point, kiez, geodata.street, geodata.number);
  }
}

class LocationMarker extends Marker {
  LocationMarker(LatLng point)
      : super(
            point: point,
            builder: (context) => DecoratedBox(
                key: Key('location marker'),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: CampaignTheme.darkorange,
                    boxShadow: [
                      BoxShadow(blurRadius: 4.0, offset: Offset(-2.0, 2.0))
                    ]),
                child: Icon(Icons.supervised_user_circle, size: 30.0)));
}

class Location {
  String? description;
  LatLng? coordinates;
  Kiez? kiez;
  String? street;
  String? number;

  Location(
      this.description, this.coordinates, this.kiez, this.street, this.number);
}
