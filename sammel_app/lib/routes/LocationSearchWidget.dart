import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:mapbox_search/mapbox_search.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

// ignore: must_be_immutable
class LocationSearchWidget extends StatefulWidget {
  late Function onFinish;
  late Function isSearching;

  LocationSearchWidget(
      {required this.onFinish, required this.isSearching, Key? key})
      : super(key: key);

  @override
  State<LocationSearchWidget> createState() => LocationSearchWidgetState();
}

class LocationSearchWidgetState extends State<LocationSearchWidget> {
  final searchInputController = TextEditingController();

  // Using mapbox for now, since nominatim should not be used with autocomplete.
  // Maybe in the future it would make sense to deploy our own nominatim instance
  // or use overpass.
  // Or we implement a soluten without autocomplete and use the open nominatim api,
  // the nominatim search is not that great though.
  final placesSearch = PlacesSearch(
      apiKey:
          'pk.eyJ1IjoiYW55a2V5IiwiYSI6ImNrM3JkZ2IwMDBhZHAzZHBpemswd3F3MjYifQ.RLinVZ2-Vdp9JwErHAJz6w',
      limit: 5,
      country: 'DE');
  ItemScrollController _scrollController = ItemScrollController();
  List<MapBoxPlace>? places;
  MapBoxPlace? chosenPlace;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        TextField(
          controller: searchInputController,
          key: Key('faq search input'),
          maxLines: 1,
          decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              suffixIcon: IconButton(
                  key: Key('faq search clear button'),
                  icon: Icon(Icons.clear),
                  onPressed: () {
                    setState(() {
                      searchInputController.clear();
                      places = null;
                    });
                    widget.isSearching(false);
                  }),
              contentPadding: EdgeInsets.symmetric(horizontal: 10.0),
              border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(20.0)),
              hintText: 'Durchsuchen'.tr()),
          onChanged: (text) {
            // We want to pass the center of berlin as location, because we cannot
            // pass a bounding box, so we pass coordinates for biased search
            if (text != '') {
              placesSearch
                  .getPlaces(text,
                      location: Location(lat: 52.520008, lng: 13.404954))
                  .then((value) {
                setState(() => places = value);
                widget.isSearching(true);
              });
            } else {
              widget.isSearching(false);
            }
          },
        ),
        if (places != null)
          Container(
            child: ListView.separated(
              // itemScrollController: _scrollController,
              itemBuilder: ((context, index) => ListTile(
                  title: Text(places![index].placeName!),
                  onTap: () {
                    widget.onFinish(places![index]);
                    widget.isSearching(false);

                    setState(() {
                      searchInputController.clear();
                      places = null;
                    });
                  })),
              separatorBuilder: (context, index) {
                return Divider();
              },
              itemCount: places!.length,
              padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
            ),
            height: 300,
            width: 350,
          )
      ]),
    );
  }
}
