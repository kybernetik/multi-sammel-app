import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:sammel_app/model/User.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/PushNotificationManager.dart';
import 'package:sammel_app/services/StammdatenService.dart';
import 'package:sammel_app/services/StorageService.dart';
import 'package:sammel_app/services/UserService.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';
import 'package:sammel_app/shared/KiezPicker.dart';
import 'package:sammel_app/shared/showUsernameDialog.dart';
import 'package:url_launcher/url_launcher.dart';

import '../model/Initiative.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  var init = false;
  StorageService? storageService;
  AbstractUserService? userService;
  AbstractInitiativesService? initiativesService;
  AbstractPushNotificationManager? pushNotificationManager;
  User? user;
  String? token;
  List<String> myKieze = [];
  List<Initiative> myInitiatives = [];
  String interval = 'lade...';

  static const intervalOptions = ['sofort', 'täglich', 'wöchentlich', 'nie'];
  static const languageOptions = ['de', 'en', 'ru', 'fr'];

  var languages = {
    'de': 'deutsch',
    'en': 'englisch',
    'ru': 'русский',
    'fr': 'français',
  };

  @override
  Widget build(BuildContext context) {
    if (init == false) {
      storageService = Provider.of<StorageService>(context);
      userService = Provider.of<AbstractUserService>(context);
      initiativesService = Provider.of<AbstractInitiativesService>(context);
      pushNotificationManager =
          Provider.of<AbstractPushNotificationManager>(context);
      userService!.user.listen((user) => setState(() => this.user = user));
      initiativesService!
          .loadInitiatives()
          .then((List<Initiative>? initiatives) => {
                storageService!.loadInitiativeIds().then((List<int>? ids) => {
                      if (initiatives != null && ids != null)
                        {
                          setState(() => myInitiatives = ids
                              .map((id) =>
                                  initiatives.firstWhere((ini) => ini.id == id))
                              .toList())
                        }
                      else
                        {setState(() => myInitiatives = [])}
                    })
              });
      pushNotificationManager!.pushToken
          .then((token) => setState(() => this.token = token));
      storageService!
          .loadNotificationInterval()
          .then((String? pref) => setState(() => interval = pref ?? 'sofort'));
      storageService!
          .loadMyKiez()
          .then((List<String>? kieze) => setState(() => myKieze = kieze ?? []));
      storageService!
          .loadMyKiez()
          .then((List<String>? kieze) => setState(() => myKieze = kieze ?? []));
      init = true;
    }
    var kiezeCaption = myKieze.join(', ');

    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(20.0),
          child: ListView(
            children: [
              SettingsItem(
                  title: 'Sprache',
                  onPressed: showLanguageDialog,
                  value: languages[
                          EasyLocalization.of(context)?.locale.languageCode] ??
                      'Keine'),
              SettingsItem(
                  title: "Dein Name",
                  value: user?.name ?? '',
                  onPressed: (context) =>
                      showUsernameDialog(context: context, hideHint: true)),
              SettingsItem(
                  title: 'Dein Kiez',
                  value: kiezeCaption,
                  onPressed: (_) => showKiezPicker(),
                  explanation:
                      'Mit deiner Kiez-Auswahl bestimmst du für welche Gegenden du über neue Aktionen informiert werden willst.'),
              SettingsItem(
                  title: 'Deine Benachrichtigungen',
                  value: interval.tr(),
                  onPressed: (context) => showNotificationDialog(context),
                  explanation:
                      'Wie oft und aktuell willst du über neue Sammel-Aktionen in deinem Kiez informiert werden?'),
              SettingsItem(
                  title: 'Deine Initiativen',
                  value: myInitiatives.isEmpty
                      ? "Keine".tr()
                      : myInitiatives.map((ini) => ini.shortName).join(", "),
                  onPressed: (context) => showInitiativesDialog(context),
                  explanation:
                      'Von welchen Initiativen möchtest du informiert werden?'),
              InfoItem(
                  title: 'Deine Daten',
                  onPressed: (context) => showPrivacyDialog(context)),
              InfoItem(
                  title: 'Über diese App',
                  onPressed: (context) => showAboutDialog(context)),
              SizedBox(height: 30),
              SelectableText('User-ID: ${user?.id}',
                  textAlign: TextAlign.center),
            ],
          )),
    );
  }

  showKiezPicker() async {
    var selection = (await KiezPicker(
                (await Provider.of<StammdatenService>(context, listen: false)
                        .kieze)
                    .where((kiez) => myKieze.contains(kiez.name))
                    .toSet())
            .showKiezPicker(context))
        ?.map((kiez) => kiez.name)
        .toList();

    if (selection != null && !ListEquality().equals(selection, myKieze)) {
      renewTopicSubscriptions(selection, interval);
      storageService?.saveMyKiez(selection);
      setState(() => myKieze = selection);
    }
  }

  showLanguageDialog(BuildContext context) async {
    String? selection = await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
            key: Key('language selection dialog'),
            contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
            titlePadding: EdgeInsets.all(15.0),
            title: const Text('Sprache').tr(),
            children: []..addAll(languageOptions.map((option) => RadioListTile(
                  groupValue: EasyLocalization.of(context)!.locale.languageCode,
                  value: option,
                  title: Text(languages[option]!),
                  onChanged: (selected) => Navigator.pop(context, selected),
                )))));

    if (selection != null)
      EasyLocalization.of(context)!.setLocale(Locale(selection));
  }

  showNotificationDialog(BuildContext context) async {
    String? selection = await showDialog(
        context: context,
        builder: (context) => SimpleDialog(
            key: Key('notification selection dialog'),
            contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
            titlePadding: EdgeInsets.all(15.0),
            title: Text(
                    'Wie häufig möchtest du Infos über anstehende Aktionen bekommen?')
                .tr(),
            children: []..addAll(intervalOptions.map((option) => RadioListTile(
                  groupValue: interval,
                  value: option,
                  title: Text(option).tr(),
                  onChanged: (selected) => Navigator.pop(context, selected),
                )))));

    if (selection != null && selection != interval) {
      renewTopicSubscriptions(myKieze, selection);
      storageService?.saveNotificationInterval(selection);
      setState(() => interval = selection);
    }
  }

  showInitiativesDialog(BuildContext context) async {
    // maybe refactor with initiativesSelection from ActionEditor.dart
    List<Initiative> allInitiatives =
        await initiativesService!.loadInitiatives();
    List<Initiative> selectedInitiatives = myInitiatives;
    await showDialog<String>(
        context: context,
        builder: (context) =>
            StatefulBuilder(builder: (context, setDialogState) {
              return SimpleDialog(
                  key: Key('initiatives selection dialog'),
                  contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
                  titlePadding: EdgeInsets.all(15.0),
                  title: const Text('Wähle Initiativen').tr(),
                  children: []
                    ..addAll(allInitiatives.map((ini) => CheckboxListTile(
                          value: selectedInitiatives.contains(ini),
                          title: Text(ini.longName),
                          onChanged: (bool? checked) {
                            setDialogState(() {
                              if (checked == null || checked == false) {
                                selectedInitiatives.remove(ini);
                              } else {
                                selectedInitiatives.add(ini);
                              }
                            });
                          },
                        )))
                    ..add(TextButton(
                        child: Text('Fertig').tr(),
                        style: CampaignTheme.yesButtonStyle,
                        onPressed: () => Navigator.pop(context))));
            }));

    setState(() {
      myInitiatives = selectedInitiatives;
      storageService!
          .saveInitiativeIds(myInitiatives.map((ini) => ini.id).toList());
    });
  }

  void renewTopicSubscriptions(List<String> newKieze, String newInterval) {
    if (interval != "nie")
      pushNotificationManager?.unsubscribeFromKiezActionTopics(
          myKieze, interval);
    if (newInterval != "nie")
      pushNotificationManager?.subscribeToKiezActionTopics(
          newKieze, newInterval);
  }
}

showNotificationInfoDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) => SimpleDialog(
              key: Key('notification info dialog'),
              contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
              titlePadding: EdgeInsets.all(15.0),
              title: Text('Benachrichtigungs-Einstellungen').tr(),
              children: [
                Image.asset(
                  'assets/images/housy_info.png',
                  height: 250,
                ),
                Container(
                    padding: EdgeInsets.all(10),
                    child: Text(Platform.isIOS
                            ? 'Wenn du Benachrichtigungen leise stellen oder bestimmte Benachrichtigungs-Arten ganz ausstellen willst, dann tippe auf die drei Punkte in einer Benachrichtigung die du bekommen hast und du gelangst zu den Benachrichtigungseinstellungen für diese App.'
                            : 'Wenn du Benachrichtigungen leise stellen oder bestimmte Benachrichtigungs-Arten ganz ausstellen willst, dann tippe einfach lange auf eine Benachrichtigung die du bekommen hast und du gelangst zu den Benachrichtigungseinstellungen für diese App.')
                        .tr()),
                TextButton(
                    child: Text('Okay', textAlign: TextAlign.end).tr(),
                    onPressed: () => Navigator.pop(context))
              ]));
}

showPrivacyDialog(BuildContext context) {
  showDialog(
      context: context,
      builder: (context) => SimpleDialog(
              key: Key('privacy dialog'),
              contentPadding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0.0),
              titlePadding: EdgeInsets.all(15.0),
              title: Text('Datenschutz').tr(),
              children: [
                Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                            'Alle Daten, die du in die App eingibst werden ausschließlich auf Systemem der Deutsche Wohnen & Co. Enteignen - Kampagne gespeichert und nur für die App und die Kampagne verwendet. Beachte jedoch, dass viele Daten, die du eingibst von anderen Nutzer*innen der App gelesen werden können. Chat-Nachrichten sind ausschließlich lesbar für alle Teilnehmer*innen des Chats zum Zeitpunkt der Nachricht.\n\nFür die Funktion der Push-Nachrichten sind wir auf den Einsatz einer Zustell-Infrastruktur von Google und ggf. Apple angewiesen. Daten die auf diesem Weg transportiert werden, werden verschlüsselt übertragen. Wenn du möchtest, dass alle persönlichen Daten, die du eingetragen hast gelöscht werden, schreibe uns bitte eine Mail an app@dwenteignen.de.')
                        .tr()),
                TextButton(
                    style: CampaignTheme.yesButtonStyle,
                    child: Text('Okay', textAlign: TextAlign.end).tr(),
                    onPressed: () => Navigator.pop(context))
              ]));
}

class SettingsItem extends StatelessWidget {
  final String title;
  final Function(BuildContext) onPressed;
  final String explanation;
  final String value;

  SettingsItem(
      {required this.title,
      required this.onPressed,
      required this.value,
      this.explanation = ''});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () => onPressed(context),
        child: Container(
            padding:
                EdgeInsets.only(top: 5.0, left: 10.0, right: 10.0, bottom: 0.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title, style: CampaignTheme.headingStyle).tr(),
                  Container(
                      decoration: BoxDecoration(
                          border: Border(
                              bottom:
                                  BorderSide(color: CampaignTheme.darkorange))),
                      child: Row(children: [
                        Expanded(
                            child: Text(
                          value,
                          overflow: TextOverflow.fade,
                          style: TextStyle(color: CampaignTheme.black),
                        )),
                        Icon(
                          Icons.edit_sharp,
                          color: CampaignTheme.black,
                        )
                      ])),
                ]..addAll(explanation != ''
                    ? [
                        SizedBox(width: double.infinity, height: 5),
                        Text(explanation,
                                style: TextStyle(
                                    fontSize: 10, color: CampaignTheme.black))
                            .tr()
                      ]
                    : []))));
  }
}

class InfoItem extends StatelessWidget {
  final String title;
  final Function(BuildContext) onPressed;

  InfoItem({required this.title, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () => onPressed(context),
        child: Container(
            padding:
                EdgeInsets.only(top: 0.0, left: 10.0, right: 10.0, bottom: 0.0),
            child: Row(children: [
              Text(title,
                  overflow: TextOverflow.fade,
                  style: TextStyle(
                      color: CampaignTheme.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 12)),
              SizedBox(width: 5),
              Icon(
                Icons.info,
                color: CampaignTheme.black,
              )
            ])));
  }
}

showAboutDialog(BuildContext context) async {
  final packageInfo = await PackageInfo.fromPlatform();
  showDialog(
      context: context,
      builder: (context) => AboutDialog(
            applicationName: 'Deutsche Wohnen & Co. Enteignen',
            applicationIcon:
                Image.asset('assets/images/logo_transparent.png', width: 40.0),
            applicationVersion:
                '${packageInfo.version} (${packageInfo.buildNumber})',
            children: [
              Text('Diese App wurde von einem kleinen Team enthusiastischer IT-Aktivist*innen für die Deutsche Wohnen & Co. Enteignen - Kampagne entwickelt und steht unter einer freien Lizenz.\n\nWenn du Interesse daran hast diese App für dein Volksbegehren einzusetzen, dann schreib uns doch einfach eine Mail oder besuche uns auf unserer Webseite. So kannst du uns auch Fehler und Probleme mit der App melden.')
                  .tr(),
              SizedBox(height: 15.0),
              Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
                RichText(
                    text: TextSpan(
                        text: 'Gitlab-Repository',
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.indigo,
                            decoration: TextDecoration.underline),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => launch(
                              'https://gitlab.com/kybernetik/sammel-app'))),
                RichText(
                    text: TextSpan(
                        text: 'app@dwenteignen.de',
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Colors.indigo,
                            decoration: TextDecoration.underline),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => launch('mailto:app@dwenteignen.de')))
              ])
            ],
          ));
}
