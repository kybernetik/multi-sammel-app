import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:provider/provider.dart';
import 'package:sammel_app/model/Evaluation.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/model/ListLocation.dart';
import 'package:sammel_app/model/Termin.dart';
import 'package:sammel_app/model/TermineFilter.dart';
import 'package:sammel_app/model/User.dart';
import 'package:sammel_app/routes/ActionEditor.dart';
import 'package:sammel_app/routes/ActionMap.dart';
import 'package:sammel_app/routes/EvaluationForm.dart';
import 'package:sammel_app/services/ChatMessageService.dart';
import 'package:sammel_app/services/ErrorService.dart';
import 'package:sammel_app/services/ListLocationService.dart';
import 'package:sammel_app/services/PlacardsService.dart';
import 'package:sammel_app/services/RestFehler.dart';
import 'package:sammel_app/services/StorageService.dart';
import 'package:sammel_app/services/TermineService.dart';
import 'package:sammel_app/services/UserService.dart';
import 'package:sammel_app/shared/CampaignTheme.dart';
import 'package:uni_links/uni_links.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';

import 'ActionDetailsPage.dart';
import 'ActionEditor.dart';
import 'ActionList.dart';
import 'ActionMap.dart';
import 'FilterWidget.dart';

class TermineSeite extends StatefulWidget {
  final Function(LatLng) switchToActionCreator;
  final Function(LatLng) switchToListLocationCreator;

  TermineSeite(
      {Key? key,
      required this.switchToActionCreator,
      required this.switchToListLocationCreator})
      : super(key: key ?? Key('action page'));

  @override
  TermineSeiteState createState() => TermineSeiteState();
}

class TermineSeiteState extends State<TermineSeite>
    with SingleTickerProviderStateMixin {
  static var filterKey = GlobalKey();
  AbstractTermineService? termineService;
  AbstractListLocationService? listLocationService;
  StorageService? storageService;
  ChatMessageService? chatMessageService;
  late AbstractPlacardsService placardService;
  final MapController mapController = MapController();
  bool _initialized = false;

  List<Termin> termine = [];
  List<ListLocation> listLocations = [];

  FilterWidget? filterWidget;

  List<int> myActions = [];
  User? me;

  int navigation = 0;
  late AnimationController _animationController;
  Animation<Offset>? _slide;
  Animation<double>? _fade;
  bool swipeLeft = false;
  var globalActionMapKey = GlobalKey<ActionMapState>(debugLabel: 'action map');

  late StreamSubscription<Uri?> uniLinkListener;
  bool showSearchBar = false;
  List<ListLocation>? foundListLocations;

  final searchInputController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 150),
      vsync: this,
    );
    _fade = Tween<double>(
      begin: 1,
      end: 0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeIn,
    ));
  }

  @override
  Widget build(BuildContext context) {
    if (!_initialized) intialize(context);

    _slide = Tween<Offset>(
      begin: Offset.zero,
      end: Offset(swipeLeft ? -0.5 : 0.5, 0),
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Curves.easeIn,
    ));

    var actionListView = Column(children: [
      Expanded(
          child: ActionList(termine, isMyAction, isPastAction, iAmParticipant,
              openTerminDetails,
              key: Key('action list')))
    ]);
    var actionMapView = Stack(children: [
      ActionMap(
        key: globalActionMapKey,
        termine: termine,
        listLocations: listLocations,
        myUserId: me?.id,
        isMyAction: isMyAction,
        isPastAction: isPastAction,
        iAmParticipant: iAmParticipant,
        openActionDetails: openTerminDetails,
        switchToActionCreator: widget.switchToActionCreator,
        switchToListLocationCreator: widget.switchToListLocationCreator,
        afterListLocationEdit: afterListLocationEdit,
        mapController: mapController,
      ),
      if (foundListLocations != null)
        Align(
          alignment: Alignment.topLeft,
          child: Padding(
              padding:
                  EdgeInsets.only(top: 70, left: 15, right: 15, bottom: 75),
              child: Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      blurRadius: 5.0,
                      color: Color.fromARGB(255, 187, 187, 187))
                ]),
                child: foundListLocations?.length == 0
                    ? Padding(
                        padding: EdgeInsets.all(15.0),
                        child: Text('Keine Orte gefunden',
                            style: TextStyle(
                              fontSize: 18.0,
                            )).tr())
                    : ListView.separated(
                        itemBuilder: ((context, index) => ListTile(
                            title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    foundListLocations![index].name ??
                                        foundListLocations![index].street ??
                                        'Keine Angabe'.tr(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 5),
                                  Text('Initiativen: '.tr() +
                                      foundListLocations![index]
                                          .initiatives
                                          .map((ini) => ini.shortName)
                                          .join(', ')),
                                  SizedBox(height: 5),
                                  Text('Kontakt: '.tr() +
                                      (foundListLocations![index].contact ??
                                          'Keine Angabe'.tr()))
                                ]),
                            onTap: () {
                              showListLocationOnMap(foundListLocations![index]);

                              setState(() {
                                searchInputController.clear();
                                this.foundListLocations = null;
                                this.showSearchBar = false;
                              });
                            })),
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                        itemCount: foundListLocations!.length,
                        padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                      ),
                height: double.infinity,
                width: double.infinity,
              )),
        ),
      Align(
        alignment: Alignment(0.95, 0.95),
        child: showSearchBar
            ? Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TextField(
                  controller: searchInputController,
                  key: Key('list location search input'),
                  maxLines: 1,
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                          key: Key('list location search clear button'),
                          icon: searchInputController.text != ''
                              ? Icon(Icons.clear)
                              : Icon(Icons.arrow_back),
                          onPressed: () {
                            setState(() {
                              // If text was empty (meaning arrow back icon was show, we don't want to show search bar anymore)
                              if (searchInputController.text == '') {
                                this.showSearchBar = false;
                              }

                              searchInputController.clear();
                              this.foundListLocations = null;
                            });
                          }),
                      contentPadding: EdgeInsets.symmetric(horizontal: 15.0),
                      border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(20.0)),
                      hintText: 'Nach Soli-Ort (Name, Kontakt) suchen'.tr()),
                  onChanged: (text) {
                    if (text.length > 1) {
                      String lowerCaseText = text.toLowerCase();
                      List<ListLocation> foundListLocations = listLocations
                          .where((location) =>
                              (location.contact != null &&
                                  location.contact!
                                      .toLowerCase()
                                      .contains(lowerCaseText)) ||
                              (location.description != null &&
                                  location.description!
                                      .toLowerCase()
                                      .contains(lowerCaseText)) ||
                              (location.name != null &&
                                  location.name!
                                      .toLowerCase()
                                      .contains(lowerCaseText)) ||
                              (location.street != null &&
                                  location.street!
                                      .toLowerCase()
                                      .contains(lowerCaseText)))
                          .toList();

                      setState(
                          () => this.foundListLocations = foundListLocations);
                    } else {
                      setState(() => this.foundListLocations = null);
                    }
                  },
                ))
            : FloatingActionButton(
                onPressed: () => setState(() => this.showSearchBar = true),
                child: Icon(Icons.search),
              ),
      ),
    ]);

    return ScaffoldMessenger(
        // um Snackbar oberhalb der Footer-Buttons zu zeigen
        child: Scaffold(
      body: Container(
          child: Stack(
        alignment: Alignment.topCenter,
        children: [
          FadeTransition(
            opacity: _fade!,
            child: SlideTransition(
              position: _slide!,
              child: IndexedStack(
                  children: [actionListView, actionMapView], index: navigation),
            ),
          ),
          filterWidget!,
        ],
      )),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: CampaignTheme.darkorange,
        unselectedItemColor: Colors.white,
        currentIndex: navigation,
        onTap: switchPage,
        backgroundColor: CampaignTheme.black,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.view_list,
                  key: Key('list view navigation button')),
              label: 'Liste'.tr()),
          BottomNavigationBarItem(
              icon: Icon(Icons.map, key: Key('map view navigation button')),
              label: 'Karte'.tr())
        ],
      ),
    ));
  }

  switchPage(index) async {
    if (index == navigation) return;
    setState(() => swipeLeft = index > navigation);
    await _animationController.forward();
    setState(() {
      navigation = index;
      swipeLeft = !swipeLeft;
    });
    await _animationController.reverse();
  }

  // funktioniert nicht im Konstruktor, weil da der BuildContext fehlt
  // und auch nicht im initState(), weil da InheritedWidgets nicht angefasst werden können
  // und didChangeDependencies() wird mehrfach aufgerufen
  void intialize(BuildContext context) {
    termineService = Provider.of<AbstractTermineService>(context);
    listLocationService = Provider.of<AbstractListLocationService>(context);
    storageService = Provider.of<StorageService>(context);
    chatMessageService = Provider.of<ChatMessageService>(context);
    filterWidget = FilterWidget(loadActionsAndListLocations, key: filterKey);

    storageService!
        .loadAllStoredActionIds()
        .then((ids) => setState(() => myActions = ids));

    placardService = Provider.of<AbstractPlacardsService>(context);

    Provider.of<AbstractUserService>(context)
        .user
        .listen((user) => setState(() => me = user));

    checkDeepLinks();

    _initialized = true;
  }

  Future loadActionsAndListLocations(TermineFilter filter) async {
    print('load actions and ll');
    await loadActions(filter);
    await loadListLocations(filter.initiativenIds);
  }

  Future loadActions(TermineFilter filter) async {
    globalActionMapKey.currentState?.update(filter.initiativenIds);
    await termineService!
        .loadActions(filter)
        .then((termine) =>
            setState(() => this.termine = termine..sort(Termin.compareByStart)))
        .catchError((e, s) => ErrorService.handleError(e, s,
            context: 'Aktionen konnten nicht geladen werden.'));
  }

  // Pass intiative ids to filter them
  Future loadListLocations(List<int> intiativeIds) async {
    await listLocationService!
        .getActiveListLocations(intiativeIds)
        .then((listLocations) {
      setState(() {
        this.listLocations = listLocations;
      });
    }).catchError((e, s) => ErrorService.handleError(e, s,
            context: 'Soli Orte konnten nicht geladen werden'));
  }

  void showRestError(RestFehler e) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Aktion konnte nicht angelegt werden').tr(),
              content: SelectableText(e.message),
              actions: <Widget>[
                ElevatedButton(
                  child: Text('Okay...').tr(),
                  onPressed: () => Navigator.pop(context),
                )
              ],
            ));
  }

  openTerminDetails(Termin termin) async {
    if (termin.id == null) return;

    try {
      var terminMitDetails =
          await termineService!.getActionWithDetails(termin.id!);
      TerminDetailsCommand? command = await showActionDetailsPage(
          context,
          terminMitDetails,
          isMyAction(termin),
          participant(termin),
          joinAction,
          leaveAction);

      if (command == TerminDetailsCommand.DELETE)
        deleteAction(terminMitDetails);

      if (command == TerminDetailsCommand.EDIT)
        editAction(context, terminMitDetails);

      if (command == TerminDetailsCommand.EVALUATE)
        evaluateAction(context, terminMitDetails);

      if (command == TerminDetailsCommand.FOCUS)
        showActionOnMap(terminMitDetails);
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Aktion konnte nicht geladen werden.');
    }
  }

  bool isMyAction(Termin action) => myActions.contains(action.id);

  bool iAmParticipant(Termin action) =>
      action.participants?.map((e) => e.id).contains(me?.id) ?? false;

  editAction(BuildContext context, Termin termin) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return SimpleDialog(
              titlePadding: EdgeInsets.zero,
              title: AppBar(
                leading: null,
                automaticallyImplyLeading: false,
                title: Text('Deine Aktion bearbeiten',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 22.0,
                            color: Color.fromARGB(255, 129, 28, 98)))
                    .tr(),
              ),
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: ActionEditor(
                      initAction: termin,
                      onFinish: afterActionEdit,
                      key: Key('action editor')),
                )
              ]);
        });
  }

  afterActionEdit(List<Termin> editedAction) async {
    await saveAction(editedAction[0]);
    openTerminDetails(editedAction[0]); // recursive and I know it
  }

  Future<void> saveAction(Termin editedAction) async {
    try {
      String? token = await storageService!.loadActionToken(editedAction.id!);
      if (token == null) throw Exception('Fehlende Authorisierung zu Aktion');
      await termineService!.saveAction(editedAction, token);
      setState(() => updateAction(editedAction, false));
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Aktion konnte nicht gespeichert werden.');
    }
  }

  Future evaluateAction(BuildContext context, Termin termin) async {
    await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return SimpleDialog(
              contentPadding: EdgeInsets.zero,
              titlePadding: EdgeInsets.zero,
              title: AppBar(
                leading: null,
                automaticallyImplyLeading: false,
                title: Text('Über Aktion berichten',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 22.0))
                    .tr(),
              ),
              children: <Widget>[
                Container(
                    color: CampaignTheme.white,
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: EvaluationForm(termin,
                        onFinish: afterActionEvaluation,
                        key: Key('evaluation editor')))
              ]);
        });
  }

  Future afterActionEvaluation(Evaluation evaluation, Termin termin) async {
    Navigator.pop(context, false);

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Vielen Dank, dass Du Eure Erfahrungen geteilt hast.'.tr(),
          style: TextStyle(color: Colors.black87)),
      behavior: SnackBarBehavior.floating,
      duration: Duration(seconds: 4),
      backgroundColor: Color.fromARGB(220, 255, 255, 250),
    ));
    await saveEvaluation(evaluation);

    // If initiative has another way for users to register signatures, we want
    // to show a second dialog including a link
    // If initiative has a custom way a link is saved in the db
    List<Initiative> inisWithRegisterSignaturesUrl = termin.initiatives
        .where((ini) => ini.registerSignaturesUrl != null)
        .toList();

    if (inisWithRegisterSignaturesUrl.isNotEmpty) {
      await showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
                contentPadding: EdgeInsets.zero,
                titlePadding: EdgeInsets.zero,
                title: AppBar(
                  leading: null,
                  automaticallyImplyLeading: false,
                  title: Text('Unterschriften eintragen',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 22.0))
                      .tr(),
                ),
                children: [
                  Container(
                      color: CampaignTheme.white,
                      padding: EdgeInsets.all(30.0),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Tausend Dank!',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22.0))
                                .tr(),
                            SizedBox(
                              height: 15.0,
                            ),
                            Text('Dein Feedback ist bei uns angekommen.',
                                    textAlign: TextAlign.left)
                                .tr(),
                            SizedBox(
                              height: 15.0,
                            ),
                            Text('Hast du Lust, deine Unterschriften auf der Website des Volksentscheid einzutragen, um den Sammelbalken steigen zu lassen?')
                                .tr(),
                            SizedBox(
                              height: 15.0,
                            ),
                            for (Initiative ini
                                in inisWithRegisterSignaturesUrl)
                              Column(
                                children: [
                                  Text('Für ${ini.longName}:'),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      TextButton(
                                        key: Key('register signatures button'),
                                        style: CampaignTheme.yesButtonStyle,
                                        child: Text('Unterschriften eintragen')
                                            .tr(),
                                        onPressed: () =>
                                            launch(ini.registerSignaturesUrl!),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                ],
                              )
                          ]))
                ]);
          });
    }
  }

  Future<void> saveEvaluation(Evaluation evaluation) async {
    try {
      await termineService?.saveEvaluation(evaluation);
      await storageService?.markActionIdAsEvaluated(evaluation.terminId!);
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Evaluation konnte nicht gespeichert werden.');
    }
    return;
  }

  Future<void> deleteAction(Termin action) async {
    if (action.id == null) return;
    String? token = await storageService!.loadActionToken(action.id!);
    try {
      if (token == null) throw Exception('Fehlende Authorisierung zu Aktion');
      await termineService?.deleteAction(action, token);
      storageService!.deleteActionToken(action.id!);
      setState(() => updateAction(action, true));
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Aktion konnte nicht gelöscht werden.');
    }
  }

  updateAction(Termin updatedAction, bool remove) {
    var index =
        termine.indexWhere((Termin action) => action.id == updatedAction.id);

    if (index == -1) return;

    if (remove) {
      termine.removeAt(index);
      myActions.remove(updatedAction.id);
    } else {
      termine[index] = updatedAction;
      termine.sort(Termin.compareByStart);
    }
  }

  createAndAddAction(Termin action) async {
    try {
      Termin actionWithId = await createNewAction(action);
      myActions.add(actionWithId.id!);
      setState(() {
        termine
          ..add(actionWithId)
          ..sort(Termin.compareByStart);
      });
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Aktion konnte nicht erzeugt werden.');
    }
  }

  Future<Termin> createNewAction(Termin action) async {
    String uuid = Uuid().v1();
    Termin actionWithId = await termineService!.createAction(action, uuid);
    storageService?.saveActionToken(actionWithId.id!, uuid);
    return actionWithId;
  }

  createAndAddListLocation(ListLocation listLocation) async {
    try {
      ListLocation? createdListLocation =
          await createNewListLocation(listLocation);

      if (createdListLocation != null) {
        setState(() {
          listLocations..add(createdListLocation);
        });
      }
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Soli-Ort konnte nicht erzeugt werden.');
    }
  }

  Future<ListLocation?> createNewListLocation(ListLocation listLocation) async {
    ListLocation? createdListLocation =
        await listLocationService!.createListLocation(listLocation);

    return createdListLocation;
  }

  afterListLocationEdit(ListLocation editedListLocation) async {
    await saveListLocation(editedListLocation);
  }

  Future<void> saveListLocation(ListLocation editedListLocation) async {
    try {
      await listLocationService!.saveListLocation(editedListLocation);
      setState(() => updateListLocation(editedListLocation));
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Soli-Ort konnte nicht gespeichert werden.');
    }
  }

  updateListLocation(ListLocation updatedListLocation) {
    var index = listLocations.indexWhere((ListLocation listLocation) =>
        listLocation.id == updatedListLocation.id);

    if (index == -1) return;

    listLocations[index] = updatedListLocation;
  }

  void showActionOnMap(Termin action) {
    setState(() => navigation = 1);
    mapController.move(LatLng(action.latitude, action.longitude), 15.0);
  }

  void showListLocationOnMap(ListLocation listLocation) {
    setState(() => navigation = 1);
    mapController.move(
        LatLng(listLocation.latitude, listLocation.longitude), 20.0);
  }

  Future<void> joinAction(Termin action) async {
    if (action.id == null || me == null) return;
    await termineService?.joinAction(action.id!);
    setState(() => termine
        .where((t) => t.id == action.id)
        .forEach((Termin t) => t.participants?.add(me!)));
  }

  Future<void> leaveAction(Termin action) async {
    if (action.id == null || me == null) return;
    await termineService?.leaveAction(action.id!);
    setState(() {
      var actionFromList = termine.firstWhere((t) => t.id == action.id);
      actionFromList.participants?.removeWhere((user) => user.id == me!.id);
    });
  }

  bool participant(Termin termin) =>
      termin.participants?.map((e) => e.id).contains(me?.id) ?? false;

  void zeigeAktionen(String title, List<Termin> actions) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Scaffold(
                appBar: AppBar(title: Text(title)),
                body: ActionList(actions, isMyAction, isPastAction,
                    iAmParticipant, openTerminDetails))));
  }

  checkDeepLinks() async {
    registerUriListener(uriLinkStream);
    showAction(await getInitialUri());
  }

  registerUriListener(Stream<Uri?> linkStream) {
    uniLinkListener = linkStream.listen((final Uri? uri) => showAction(uri));
  }

  showAction(Uri? uri) {
    if (uri?.queryParameters['aktion'] == null) return;
    final int? id = int.tryParse(uri!.queryParameters['aktion']!);
    if (id == null) {
      ErrorService.pushError("Ungültige Aktions-ID",
          "Die Nummer der Aktion in dem Link ist ungültig. Möglicherweise wurde die Aktion bereits gelöscht.");
      return;
    }
    termineService!.loadAndShowAction(id);
  }

  @override
  void dispose() {
    try {
      uniLinkListener.cancel();
    } catch (_) {}
    super.dispose();
  }
}

class ButtonRow extends StatelessWidget {
  final List<Widget> widgets;

  ButtonRow(this.widgets);

  @override
  Widget build(BuildContext context) {
    return Row(
        children: widgets
            .expand((widget) => [SizedBox(width: 5.0), widget])
            .skip(1)
            .toList());
  }
}

AlertDialog confirmDeleteDialog(BuildContext context) => AlertDialog(
        key: Key('deletion confirmation dialog'),
        title: Text('Aktion Löschen').tr(),
        content: Text('Möchtest du diese Aktion wirklich löschen?').tr(),
        actions: [
          TextButton(
              key: Key('delete confirmation yes button'),
              style: CampaignTheme.warningButtonStyle,
              child: Text('Ja').tr(),
              onPressed: () => Navigator.pop(context, true)),
          ElevatedButton(
            key: Key('delete confirmation no button'),
            style: CampaignTheme.cancelButtonStyle,
            child: Text('Nein').tr(),
            onPressed: () => Navigator.pop(context, false),
          ),
        ]);

// If no end time exists we check, if it is next day
bool isPastAction(Termin action) => action.isPastAction();
