import 'dart:async';

import 'package:quiver/strings.dart';
import 'package:sammel_app/model/FAQItem.dart';
import 'package:sammel_app/services/BackendService.dart';
import 'package:sammel_app/services/StorageService.dart';
import 'package:sammel_app/services/UserService.dart';

import 'ErrorService.dart';

abstract class AbstractFAQService {
  Stream<List<FAQItem>?> getSortedFAQ(String? search);
}

mixin FAQSorter {
  List<FAQItem>? sortItems(String? search, List<FAQItem>? items) {
    if (items == null) return null;
    if (search == null) search = '';

    if (search == '') {
      List<FAQItem> orderedItems =
          List<FAQItem>.of(items); // Kopie zum sortieren
      orderedItems.sort((item1, item2) => item1.id < item2.id ? -1 : 1);
      return orderedItems;
    }

    var searchWords = search
        .split(RegExp(r"\s+"))
        .map((word) => word.toLowerCase())
        .where((word) => isNotBlank(word));

    // Erzeugt eine Map die für jedes item das Gewicht (~ Anzahl Treffer) speichert
    Map<FAQItem, int> weight =
        Map.fromIterable(items, key: (item) => item, value: (_) => 0);

    // Suche in Schlagwörtern
    searchWords.forEach((word) {
      items.forEach((item) {
        for (var tag in item.tags) {
          if (tag.toLowerCase().contains(word)) {
            // Schlagwörter haben schweres Gewicht
            weight[item] = weight[item]! + 100;
            break; // nicht mehrere Treffer pro Stichwort
          }
        }
      });
    });

    // Suche im Titel
    searchWords.forEach((word) {
      items.forEach((item) {
        if (item.title.toLowerCase().contains(word))
          // Titel-Treffer haben mittleres Gewicht
          weight[item] = weight[item]! + 10;
      });
    });

    // Suche im Text
    searchWords.forEach((word) {
      items.forEach((item) {
        if (item.full.toLowerCase().contains(word))
          // niedriges Gewicht für Text-Treffer
          weight[item] = weight[item]! + 1;
      });
    });

    List<FAQItem> orderedItems = List<FAQItem>.of(items); // Kopie zum sortieren
    orderedItems.sort((item1, item2) =>
        Comparable.compare(weight[item1]!, weight[item2]!) * -1);

    return orderedItems;
  }
}

class FAQService extends BackendService
    with FAQSorter
    implements AbstractFAQService {
  StorageService storageService;

  StreamController<List<FAQItem>?> controller = StreamController.broadcast();
  late Stream<List<FAQItem>?> stream;
  List<FAQItem>? latest;

  FAQService(
      this.storageService, AbstractUserService userService, Backend backend)
      : super(userService, backend) {
    stream = controller.stream;
    stream.listen((faq) => latest = faq);
    storageService
        .loadFAQ()
        .then((value) => value ?? List<FAQItem>.empty())
        .then((faq) => controller.add(faq));
    updateFAQfromServer();
  }

  @override
  Stream<List<FAQItem>?> getSortedFAQ(String? search) {
    // ignore: close_sinks
    StreamController<List<FAQItem>?> sortedController = StreamController();
    if (latest != null) sortedController.add(latest);
    sortedController.addStream(stream).whenComplete(() => controller.close());
    return sortedController.stream.map((items) => sortItems(search, items));
  }

  updateFAQfromServer() async {
    var serverTimestamp = (await backend.getServerHealth()).faqTimestamp;
    var localTimestamp = await storageService.loadFAQTimestamp();

    if (localTimestamp != null &&
        (serverTimestamp == null || localTimestamp.isAfter(serverTimestamp)))
      return;

    try {
      final response = await get('service/faq', appAuth: true);
      final faqFromServer = (response.body as List)
          .map((item) => FAQItem.fromJson(item))
          .toList();
      storageService.saveFAQ(faqFromServer);
      storageService.saveFAQTimestamp(DateTime.now());
      controller.add(faqFromServer);
      controller.close();
    } catch (e) {
      ErrorService.handleError(e, StackTrace.current);
    }
  }
}

class DemoFAQService with FAQSorter implements AbstractFAQService {
  @override
  Stream<List<FAQItem>?> getSortedFAQ(String? search) =>
      Stream.value(sortItems(search, faqItems));

  List<FAQItem> faqItems = [
    FAQItem.short(
        1,
        'Wann geht\'s los?',
        'Am **26. Februar** ist _Sammelbeginn_.',
        1.0,
        ['Start', 'Beginn', 'Sammelphase'],
        1),
    FAQItem(
        2,
        'Was sind die Stufen eines Volksbegehrens?',
        '''Jedes Volksbegehren läuft in 3 Stufen ab.
Diese sind:''',
        '''\n\n
| **Stufe** | **Erfolg** |
| ------------ | ------------ |
| Volksinitiative  | 50.000 Unterschriften  |
| Volksbegehren  | 170.000 Unterschriften  |
| Volksentscheid  | 613.000 Ja-Stimmen  |
''',
        2.0,
        [
          'Stufen',
          'Volksbegehren',
          'Volksinitiative',
          'Erfolg',
          'Unterschriften'
        ],
        null),
    FAQItem(
        3,
        'Sammeltipps',
        '''Ein kurzer und inhaltlich einfacher Satz hat sich oft bewährt.

Ein paar Beispiele:
''',
        '''> "Hast Du schon von unserem Volksbegehren gehört?“ 
> „Möchtest Du für Klimaschutz unterschreiben?“
> „Wir sammeln Unterschriften gegen Autos in der Innenstadt – möchten Sie unterschreiben?“

Nach kurzer Bedenkzeit der Person kann eine weitere erläuternder Satz nachgeschoben werden:

> „Wir sind von der Initiative für einen autofreien S-Bahn-Ring“
> „Wir sind von der Initiative Berlin Autofrei.“''',
        3.0,
        ['Tipps', 'Sammeln', 'Hinweise', 'Ansprechen'],
        3),
    FAQItem.short(
        4,
        'Feedback und Fehlermeldungen',
        '''Wenn du eine Fehler gefunden hast oder uns anderes Feedback zur App melden willst, dann schreib uns doch [per Mail](mailto:app@dwenteignen.de) oder öffne ein Bug-Ticket auf [der App-Webseite](https://www.gitlab.com/kybernetik/sammel-app)''',
        4.0,
        ['Bugs', 'Kontakt', 'Webseite', 'Email', 'E-Mail'],
        null),
    FAQItem(
        5,
        'Bürger:innenversammlung',
        '''Bürger:innenversammlung sind ein erprobtes Mittel der direkten Demokratie.

Ein Beispiel ist der Bürgerrat Klima:
''',
        '''Der Bürgerrat Klima ist eine losbasierte Bürgerversammlung unter der Schirmherrschaft von Bundespräsident a. D. Horst Köhler,[1] bestehend aus 160 Bürgerinnen und Bürgern, die an zwölf Sitzungen zwischen dem 26. April und dem 23. Juni 2021 Empfehlungen für die Klimapolitik Deutschlands erarbeiteten. Die zivilgesellschaftliche Initiative wird von zahlreichen Organisationen unterstützt. Adressat sind die Parteien des Bundestages bzw. die nächste Regierungs-Koalition, die die Empfehlungen des Bürgerrat Klima berücksichtigen und umsetzen soll. ''',
        4.0,
        ['Tipps', 'Sammeln', 'Hinweise', 'Ansprechen'],
        2),
  ];
}
