import 'dart:ui';

import 'package:http_server/http_server.dart';
import 'package:sammel_app/model/Initiative.dart';

import 'BackendService.dart';
import 'ErrorService.dart';
import 'UserService.dart';

abstract class AbstractInitiativesService extends BackendService {
  Future<List<Initiative>> loadInitiatives();

  AbstractInitiativesService(AbstractUserService userService, Backend backend)
      : super(userService, backend);
}

class InitiativesService extends AbstractInitiativesService {
  List<Initiative>? cache;

  InitiativesService(AbstractUserService userService, Backend backend)
      : super(userService, backend);

  @override
  Future<List<Initiative>> loadInitiatives() async {
    if (cache != null) return cache!;
    HttpClientResponseBody response;
    try {
      response = await get('/service/initiatives');
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Initiativen konnten nicht geladen werden.');
      return [];
    }
    final initiatives = (response.body as List)
        .map((jsonInitiative) => Initiative.fromJson(jsonInitiative))
        .toList();
    cache = initiatives;
    return initiatives;
  }
}

class DemoInitiativesService extends AbstractInitiativesService {
  late List<Initiative> initiatives;
  static Initiative expedition = Initiative(
      1,
      "Expedition Grundeinkommen",
      "Expedition",
      Color(0xFF7d69f6),
      Color(0xFF46b4b4),
      "sammeln",
      "https://expedition-grundeinkommen.de/",
      "Deutschland redet übers Grundeinkommen. Wir handeln und bringen den ersten staatlichen Modellversuch zum Grundeinkommen auf den Weg – gemeinsam mit dir per direkter Demokratie.",
      'https://www.volksentscheid-grundeinkommen.de/me/unterschriften-eintragen');
  static Initiative klimaneutral = Initiative(
    2,
    "Berlin 2030 klimaneutral",
    "2030 klimaneutral",
    Color(0xFFfc2a1c),
    null,
    "sammeln",
    "https://klimaneustart.berlin/",
    "Klimaneustart Berlin will daher mit einem Volksbegehren und -entscheid bewirken, dass wir unserer Verantwortung endlich gerecht werden, d.h Klimaneutralität bis 2030! Das Berliner Energiewendegesetz (EWG Bln) ist der gesetzliche Handlungsrahmen zu diesem Vorhaben, weshalb unsere Initiative auf dieses Gesetz abzielt.",
    "https://klimaneustart.berlin/",
  );
  static Initiative autofrei = Initiative(
      3,
      "berlin autofrei",
      "berlin autofrei",
      Color(0xFF9cb0c9),
      null,
      "sammeln",
      "https://volksentscheid-berlin-autofrei.de/",
      "Wir wollen deutlich weniger Autoverkehr innerhalb des Berliner S-Bahn-Rings. Der „Volksentscheid Berlin autofrei“ sorgt für eine gesunde, sichere und klimaschonende Stadt mit mehr Platz für alle! In Zeiten des Klimawandels und des knapper werdenden Raums in Großstädten brauchen wir eine wirksame und sozial gerechte Verkehrswende. Politiker*innen ergreifen dafür aus unserer Sicht nicht die nötigen Maßnahmen - es ist Zeit, dass sich was bewegt.",
      null);

  DemoInitiativesService(AbstractUserService userService)
      : super(userService, DemoBackend()) {
    initiatives = [expedition, klimaneutral, autofrei];
  }

  @override
  Future<List<Initiative>> loadInitiatives() async {
    return await initiatives;
  }
}
