import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:http_server/http_server.dart';
import 'package:sammel_app/model/ListLocation.dart';
import 'package:sammel_app/services/ErrorService.dart';
import 'package:sammel_app/services/InitiativesService.dart';

import 'BackendService.dart';
import 'UserService.dart';

abstract class AbstractListLocationService extends BackendService {
  Future<List<ListLocation>> getActiveListLocations(List<int> initiativeIds);
  Future<ListLocation?> createListLocation(ListLocation listLocation);
  Future<void> saveListLocation(ListLocation listLocation);
  AbstractInitiativesService initiativeService;

  AbstractListLocationService(
      this.initiativeService, AbstractUserService userService, Backend backend)
      : super(userService, backend);
}

class ListLocationService extends AbstractListLocationService {
  List<ListLocation>? cache;
  List<int>? cacheFilter;

  ListLocationService(
      initiativeService, AbstractUserService userService, Backend backend)
      : super(initiativeService, userService, backend);

  @override
  // Only initiatives are relevant in the filter
  Future<List<ListLocation>> getActiveListLocations(
      List<int> initiativeIds) async {
    // Only return cache if filter has not changed
    if (cache != null && listEquals(cacheFilter, initiativeIds)) return cache!;

    HttpClientResponseBody response;
    try {
      // First check which server version is online, to do either a post or a get request
      // NOTE: this can be removed once the breaking change is in the past
      var serverVersion = await getServerVersion();

      if (serverVersion != '1.9') {
        response = await post('/service/listlocations/actives',
            jsonEncode({'initiativenIds': initiativeIds}));
      } else {
        response = await get('/service/listlocations/actives');
      }
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Listen-Orte konnten nicht geladen werden.');
      return [];
    }

    var initiatives = await initiativeService.loadInitiatives();

    final listLocations = (response.body as List)
        .map((jsonListLocation) =>
            ListLocation.fromJson(jsonListLocation, initiatives))
        .toList();

    cache = listLocations;
    cacheFilter = initiativeIds;

    return listLocations;
  }

  @override
  Future<ListLocation?> createListLocation(ListLocation listLocation) async {
    try {
      var response =
          await post('service/listlocations/neu', jsonEncode(listLocation));

      var initiatives = await initiativeService.loadInitiatives();

      return ListLocation.fromJson(response.body, initiatives);
    } catch (e, s) {
      ErrorService.handleError(e, s,
          context: 'Eintragen von Soli-Ort ist fehlgeschlagen.');

      return null;
    }
  }

  @override
  Future<void> saveListLocation(ListLocation listLocation) async {
    print(jsonEncode(listLocation));

    await post(
        'service/listlocations/${listLocation.id}', jsonEncode(listLocation));
  }
}

class DemoListLocationService extends AbstractListLocationService {
  DemoListLocationService(
      AbstractInitiativesService initiativeService, userService)
      : super(initiativeService, userService, DemoBackend());
  List<ListLocation> listLocations = [
    ListLocation(1, 'Curry 36', 'Mehringdamm', '36', 'Immer offen',
        'Ruft mich einfach an.', 52.4935584, 13.3877282, [
      DemoInitiativesService.expedition,
      DemoInitiativesService.klimaneutral
    ]),
    ListLocation(
        2,
        'Café Kotti',
        'Adalbertstraße',
        '96',
        'Das Café ist sehr schön gelegen und hat viele Listen an mehreren Stellen ausgelegt. Ich gehe einmal die Woche hin, um sie abzuholen.',
        'Ruft mich einfach an.',
        52.5001477,
        13.4181523,
        [DemoInitiativesService.klimaneutral]),
    ListLocation(3, 'Zukunft', 'Laskerstraße', '5', 'Immer offen', null,
        52.5016524, 13.4655402, [DemoInitiativesService.autofrei]),
  ];

  @override
  Future<List<ListLocation>> getActiveListLocations(
      List<int> initiativeIds) async {
    return listLocations
        .where((location) =>
            initiativeIds.isEmpty ||
            location.initiatives.any((ini) => initiativeIds.contains(ini.id)))
        .toList();
  }

  @override
  Future<ListLocation> createListLocation(ListLocation newListLocation) {
    int highestId =
        listLocations.map((listLocation) => listLocation.id!).reduce(max);
    newListLocation.id = highestId + 1;
    listLocations.add(newListLocation);
    return Future.value(newListLocation);
  }

  @override
  Future<void> saveListLocation(ListLocation listLocation) async {
    this.listLocations[this.listLocations.indexWhere(
            (oldListLocation) => oldListLocation.id == listLocation.id)] =
        listLocation;
  }
}
