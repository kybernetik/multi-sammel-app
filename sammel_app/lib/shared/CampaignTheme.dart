import 'package:flutter/material.dart';
import 'package:sammel_app/model/Initiative.dart';
import 'package:sammel_app/model/Termin.dart';

class CampaignTheme {
  static final Color darkorange = Color.fromRGBO(255, 201, 137, 1);
  static final Color lightorange = Color.fromARGB(255, 255, 237, 210);
  static final Color white = Color.fromARGB(255, 0xff, 0xff, 0xff);
  static final Color lightgray = Color.fromARGB(255, 0xf1, 0xf1, 0xf1);
  static final Color darkgray = Color.fromARGB(255, 0xa3, 0xa3, 0xa3);
  static final Color black = Color.fromARGB(255, 0x00, 0x00, 0x00);

  // styles for new list and new action
  static final TextStyle headingStyle =
      TextStyle(color: CampaignTheme.black, fontWeight: FontWeight.bold);
  static final TextStyle detailStyle = TextStyle(
    color: CampaignTheme.black,
    fontWeight: FontWeight.bold,
  );

  static final ButtonStyle yesButtonStyle = ButtonStyle(
      foregroundColor: MaterialStateProperty.all<Color>(white),
      backgroundColor: MaterialStateProperty.all<Color>(black),
      textStyle: MaterialStateProperty.all<TextStyle>(
          TextStyle(fontWeight: FontWeight.bold)),
      minimumSize: MaterialStateProperty.all(Size(120.0, 0)),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(40))));

  static final ButtonStyle cancelButtonStyle = ButtonStyle(
      foregroundColor: MaterialStateProperty.all<Color>(black),
      backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
      textStyle: MaterialStateProperty.all<TextStyle>(TextStyle(
          decoration: TextDecoration.underline, fontWeight: FontWeight.bold)));

  static final ButtonStyle warningButtonStyle = ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color>(Colors.red));

  static ThemeData themeData = ThemeData(
    // button text color
    primaryColor: darkorange,
    accentColor: darkorange,
    appBarTheme: AppBarTheme(
        backgroundColor: lightgray,
        foregroundColor: black,
        textTheme: TextTheme(
            headline6: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20.0, color: black),
            headline5: TextStyle(color: black))),
    dialogBackgroundColor: lightgray,
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      backgroundColor: MaterialStateProperty.all<Color>(lightgray),
    )),
    iconTheme: IconThemeData(color: black),
    fontFamily: 'Cabin',
    checkboxTheme: CheckboxThemeData(
      side: MaterialStateBorderSide.resolveWith(
          (state) => BorderSide(width: 1, color: black)),
      checkColor: MaterialStateProperty.all<Color>(black),
      // overlayColor: MaterialStateProperty.all<Color>(white),
      fillColor: MaterialStateProperty.all<Color>(darkorange),
    ),
    toggleableActiveColor: darkorange,
  );

  static var menuCaption =
      TextStyle(fontSize: 16.0, color: black, fontWeight: FontWeight.bold);
  static var menuCaptionSelected = menuCaption.copyWith(color: black);

  static var menuSubCaption = TextStyle(fontSize: 14.0, color: black);

  static Color actionColor(Termin action, bool owner, bool participant) {
    final past = action.isPastAction();

    // Black is not ideal for now, since the icons are also black
    // if (owner && past) return black;
    if (owner && past) return darkgray;

    if (owner) return darkorange;

    if (participant && past) return darkgray;
    if (participant) return lightorange;

    if (past) return lightgray;
    return white;
  }

  static Color placardColor(bool owner, Initiative? initiative) {
    if (owner) return darkorange;

    // If placard was not created by user we want to render the color of the ini
    if (initiative != null) return initiative.primaryColor;

    return Colors.transparent;
  }

  static String actionUrl(int? id) => 'berliner-sammelapp.de/a?aktion=$id';
}
