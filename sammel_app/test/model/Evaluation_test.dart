import 'dart:convert';

import 'package:sammel_app/model/Evaluation.dart';
import 'package:test/test.dart';

void main() {
  test('serialisiert', () {
    expect(
        jsonEncode(Evaluation(1, 10, true, true, 20, 3.5, [], false, 3, 'jo war gut', 'aber koennte noch besser', 'nix')),
        '{'
        '"termin_id":1,'
        '"teilnehmer":10,'
        '"unterschriften":20,'
        '"bewertung":3,'
        '"stunden":3.5,'
        '"ort_typen":[],'
        '"beschreibung":"jo war gut",'
        '"verbesserung":"aber koennte noch besser",'
        '"sonstiges":"nix",'
        '"in_bewegung":false,'
        '"schaetzung":true,'
        '"fuer_alle":true,'
        '"ausgefallen":false}'
    );
  });
}
