import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:sammel_app/model/Placard.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/shared/DeserialisationException.dart';

void main() {
  group('deserialises', () {
    test('Placard without id', () {
      expect(
          jsonEncode(Placard(
                  null,
                  52.472246,
                  13.327783,
                  '12161, Friedrich-Wilhelm-Platz 57',
                  13,
                  false,
                  DemoInitiativesService.expedition)
              .toJson()),
          '{'
          '"id":null,'
          '"latitude":52.472246,'
          '"longitude":13.327783,'
          '"adresse":"12161, Friedrich-Wilhelm-Platz 57",'
          '"benutzer":13,'
          '"abgehangen":false,'
          '"initiativenId":1'
          '}');
    });

    test('filled Placard', () {
      expect(
          jsonEncode(Placard(
                  1,
                  52.472246,
                  13.327783,
                  '12161, Friedrich-Wilhelm-Platz 57',
                  11,
                  false,
                  DemoInitiativesService.klimaneutral)
              .toJson()),
          '{'
          '"id":1,'
          '"latitude":52.472246,'
          '"longitude":13.327783,'
          '"adresse":"12161, Friedrich-Wilhelm-Platz 57",'
          '"benutzer":11,'
          '"abgehangen":false,'
          '"initiativenId":2'
          '}');
    });
  });

  group('deserialises', () {
    test('throws error on missing values', () {
      var json = jsonDecode('{'
          '"id":null,'
          '"latitude":null,'
          '"longitude":null,'
          '"adresse":null,'
          '"benutzer":null,'
          '"abgehangen":null'
          '}');

      expect(
          () => Placard.fromJson(json, []),
          throwsA((e) =>
              e is DeserialisationException &&
              e.message ==
                  'Fehlende Werte: latitude, longitude, adresse, benutzer, abgehangen'));
    });

    test('filled placard', () {
      final placard = Placard.fromJson(
          jsonDecode('{'
              '"id":1,'
              '"latitude":52.472246,'
              '"longitude":13.327783,'
              '"adresse":"12161, Friedrich-Wilhelm-Platz 57",'
              '"benutzer":11,'
              '"abgehangen":false,'
              '"initiativenId":1'
              '}'),
          [
            DemoInitiativesService.autofrei,
            DemoInitiativesService.expedition,
            DemoInitiativesService.klimaneutral
          ]);

      expect(placard.id, 1);
      expect(placard.latitude, 52.472246);
      expect(placard.longitude, 13.327783);
      expect(placard.adresse, '12161, Friedrich-Wilhelm-Platz 57');
      expect(placard.benutzer, 11);
      expect(placard.initiative?.id, 1);
    });
  });
}
