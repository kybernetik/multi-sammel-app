import 'package:flutter_test/flutter_test.dart';
import 'package:http_server/http_server.dart';
import 'package:jiffy/jiffy.dart';
import 'package:mockito/mockito.dart';
import 'package:sammel_app/model/FAQItem.dart';
import 'package:sammel_app/model/Health.dart';
import 'package:sammel_app/services/FAQService.dart';

import '../shared/mocks.costumized.dart';
import '../shared/mocks.mocks.dart';
import '../shared/mocks.trainer.dart';

final _storageService = MockStorageService();
final _userService = MockUserService();
final _backend = MockBackend();

main() {
  setUp(() {
    reset(_storageService);
    reset(_userService);
    reset(_backend);
    trainUserService(_userService);
    trainBackend(_backend);
    trainStorageService(_storageService);
    when(_storageService.loadFAQ()).thenAnswer((_) => Future.value(testItems));
    when(_backend.get('service/faq', any)).thenAnswer((_) =>
        Future<HttpClientResponseBody>.value(
            trainHttpResponse(MockHttpClientResponseBody(), 200, [
          {
            "id": 4,
            "title": "Myrmica Ruginodis",
            "teaser": "Klein aber gemein",
            "order": 4.0,
            "tags": ["gelb", "braun", "giftig"]
          }
        ])));
  });

  group('mixin', () {
    test('returns null with missing faqs', () async {
      expect(TestFAQSorter().sortItems(null, null), isNull);
    });

    test('orders by number of hits in tags', () async {
      List<FAQItem> orderedItems =
          TestFAQSorter().sortItems('Ameise rot Holz', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Camponotus Ligniperdus',
            'Messor Barbarus',
            'Lasius Niger',
          ]));
    });

    test('orders by number of hits in title', () async {
      List<FAQItem> orderedItems =
          TestFAQSorter().sortItems('Lasius Niger Messor', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Lasius Niger',
            'Messor Barbarus',
            'Camponotus Ligniperdus',
          ]));
    });

    test('hits in tags outweight hits in title', () async {
      List<FAQItem> orderedItems =
          TestFAQSorter().sortItems('Holz Lasius Niger', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Camponotus Ligniperdus',
            'Lasius Niger',
            'Messor Barbarus',
          ]));
    });

    test('orders by number of hits in content text', () async {
      List<FAQItem> orderedItems = TestFAQSorter().sortItems(
          'Netter Fleißig friedlich dickste Herrscher Mitteleuropa',
          testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Messor Barbarus',
            'Camponotus Ligniperdus',
            'Lasius Niger',
          ]));
    });

    test('hits in title outweight hits in text', () async {
      List<FAQItem> orderedItems = TestFAQSorter()
          .sortItems('Netter Fleißig friedlich Ligniperdus', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Camponotus Ligniperdus',
            'Messor Barbarus',
            'Lasius Niger',
          ]));
    });

    test('is not case sensitive', () async {
      List<FAQItem> orderedItems = TestFAQSorter()
          .sortItems('RäUbEr CaMpoNoTuS lIgNiPeRdUs', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Lasius Niger',
            'Camponotus Ligniperdus',
            'Messor Barbarus',
          ]));
    });

    test('finds sub-words', () async {
      List<FAQItem> orderedItems =
          TestFAQSorter().sortItems('europa unscheinbar welt', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Lasius Niger',
            'Camponotus Ligniperdus',
            'Messor Barbarus',
          ]));
    });

    test('does not change original items order', () async {
      var items = testItems;
      TestFAQSorter().sortItems('Lasius Messor Barbarus', items);

      expect(
          items.map((item) => item.title),
          containsAllInOrder([
            'Camponotus Ligniperdus',
            'Lasius Niger',
            'Messor Barbarus',
          ]));
    });

    test('can handle multiple spaces and all kind of blanks', () async {
      List<FAQItem> orderedItems = TestFAQSorter()
          .sortItems('Lasius     Messor\n\tBarbarus', testItems)!;

      expect(
          orderedItems.map((item) => item.title),
          containsAllInOrder([
            'Messor Barbarus',
            'Lasius Niger',
            'Camponotus Ligniperdus',
          ]));
    });
  });

  group('DemoFAQService', () {
    test('delivers demo faq items', () async {
      var faq = await DemoFAQService().getSortedFAQ(null).first;

      expect(faq, isNotNull);
      expect(faq!.length, 5);
      expect(faq[0].title, 'Wann geht\'s los?');
      expect(faq[1].title, 'Was sind die Stufen eines Volksbegehrens?');
      expect(faq[2].title, 'Sammeltipps');
      expect(faq[3].title, 'Feedback und Fehlermeldungen');
    });

    test('uses sorter', () async {
      final faqUnsorted = await DemoFAQService().getSortedFAQ(null).first;
      final faqSorted = await DemoFAQService().getSortedFAQ('Erfolg').first;

      expect(faqSorted!.map((e) => e.id).toList(),
          containsAll(faqUnsorted!.map((e) => e.id)));
      expect(faqSorted.map((e) => e.id).toList(),
          isNot(containsAllInOrder(faqUnsorted.map((e) => e.id))));
    });
  });

  group('FAQService', () {
    test('loads and returns old faq from storage on start', () async {
      var faqService = FAQService(_storageService, _userService, _backend);
      var faq = await faqService.getSortedFAQ(null).first;

      expect(faq, isNotNull);
      expect(faq!.length, 3);
      expect(faq[0].id, 1);
      expect(faq[1].id, 2);
      expect(faq[2].id, 3);

      verify(_storageService.loadFAQ()).called(1);
    });

    test(
        'retrieves and returns new faq from server on start when local timestamp is null',
        () async {
      var faqService = FAQService(_storageService, _userService, _backend);

      List<FAQItem>? faq;
      faqService.getSortedFAQ(null).listen((newFaq) => faq = newFaq);
      await Future.delayed(Duration(milliseconds: 100));

      verify(_backend.get('service/faq', any)).called(1);
      expect(faq, isNotNull);
      expect(faq!.length, 1);
      expect(faq![0].id, 4);
    });

    test('stores new faq from server in storage when local timestamp is null',
        () async {
      var faqService = FAQService(_storageService, _userService, _backend);

      faqService.getSortedFAQ(null);
      await Future.delayed(Duration(milliseconds: 100));

      var faq = verify(_storageService.saveFAQ(captureAny)).captured;
      expect(faq.length, 1);
      expect((faq[0] as List<FAQItem>).length, 1);
      expect((faq[0] as List<FAQItem>)[0].id, 4);
      expect((faq[0] as List<FAQItem>)[0].title, 'Myrmica Ruginodis');
    });

    test(
        'does not retrieve new faq from server in storage when local timestamp is not null, but servers is',
        () async {
      when(_storageService.loadFAQTimestamp())
          .thenAnswer((_) => Future.value(_today()));
      var faqService = FAQService(_storageService, _userService, _backend);

      faqService.getSortedFAQ(null);
      await Future.delayed(Duration(milliseconds: 100));

      verifyNever(_backend.get('service/faq', any));
      verifyNever(_storageService.saveFAQ(captureAny));
    });

    test(
        'does not retrieve new faq from server in storage when local timestamp is newer then servers',
        () async {
      when(_storageService.loadFAQTimestamp())
          .thenAnswer((_) => Future.value(_today()));
      when(_backend.getServerHealth()).thenAnswer(
          (_) => Future.value(ServerHealth()..faqTimestamp = _yesterday()));
      var faqService = FAQService(_storageService, _userService, _backend);

      faqService.getSortedFAQ(null);
      await Future.delayed(Duration(milliseconds: 100));

      verifyNever(_backend.get('service/faq', any));
      verifyNever(_storageService.saveFAQ(captureAny));
    });

    test(
        'does not retrieve new faq from server in storage when local timestamp is newer then servers',
        () async {
      when(_storageService.loadFAQTimestamp())
          .thenAnswer((_) => Future.value(_yesterday()));
      when(_backend.getServerHealth()).thenAnswer(
          (_) => Future.value(ServerHealth()..faqTimestamp = _today()));
      var faqService = FAQService(_storageService, _userService, _backend);

      faqService.getSortedFAQ(null);
      await Future.delayed(Duration(milliseconds: 100));

      verify(_backend.get('service/faq', any)).called(1);
      verify(_storageService.saveFAQ(captureAny)).called(1);
    });
  });
}

DateTime _today() => DateTime.now();

DateTime _yesterday() => Jiffy(DateTime.now()).subtract(days: 1).dateTime;

var testItems = [
  FAQItem(1, 'Camponotus Ligniperdus', 'Die dickste Ameise', ' in Mitteleuropa',
      1.0, ['Holz', 'Ameise', 'rot', 'schwarz'], null),
  FAQItem(2, 'Lasius Niger', 'Die unscheinbaren Herrscher', ' der Krabbelwelt',
      2.0, ['Räuber', 'Erdnester', 'schwarz'], 1),
  FAQItem(
      3,
      'Messor Barbarus',
      'Netter als ihr Name vermuten lässt.',
      ' Fleißig und friedlich.',
      3.0,
      ['Sammler', 'Erdnester', 'rot', 'schwarz'],
      2),
];

class TestFAQSorter with FAQSorter {}
