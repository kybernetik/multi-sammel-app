import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sammel_app/model/ListLocation.dart';
import 'package:sammel_app/services/BackendService.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/ListLocationService.dart';

import '../shared/TestdatenVorrat.dart';
import '../shared/mocks.trainer.dart';
import '../shared/mocks.costumized.dart';
import '../shared/mocks.mocks.dart';

void main() {
  MockUserService userService = MockUserService();
  MockInitiativesService initiativesService = MockInitiativesService();

  trainUserService(userService);

  group('ListLocationService', () {
    late MockBackend backend;
    late ListLocationService service;

    setUp(() {
      backend = MockBackend();
      trainBackend(backend);
      trainInitiativesService(initiativesService);
      service = ListLocationService(
        initiativesService,
        userService,
        backend,
      );
      service.userService = userService;
    });

    test('ListLocationService returns list locations', () async {
      var cafeKottiJson = {
        'id': 2,
        'name': 'Café Kotti',
        'street': 'Adalbertstraße',
        'number': '96',
        'description':
            'Das Café ist sehr schön gelegen und hat viele Listen an mehreren Stellen ausgelegt. Ich gehe einmal die Woche hin, um sie abzuholen.',
        'contact': 'Ruft mich einfach an.',
        'latitude': 52.5001477,
        'longitude': 13.4181523
      };
      var zukunftJson = {
        'id': 3,
        'name': 'Zukunft',
        'street': 'Laskerstraße',
        'number': '5',
        'description': 'Immer offen',
        'contact': 'Ruft mich einfach an.',
        'latitude': 52.5016524,
        'longitude': 13.4655402
      };
      List<Map<String, dynamic>> listlocations = [cafeKottiJson, zukunftJson];
      when(backend.post('/service/listlocations/actives', any, any)).thenAnswer(
          (_) async => trainHttpResponse(
              MockHttpClientResponseBody(), 200, listlocations));

      List<ListLocation> ergebnis = await service.getActiveListLocations([0]);

      expect(ergebnis.length, 2);

      expect(ergebnis[0].id, 2);
      expect(ergebnis[0].name, 'Café Kotti');
      expect(ergebnis[0].street, 'Adalbertstraße');
      expect(ergebnis[0].number, '96');
      expect(ergebnis[0].description,
          'Das Café ist sehr schön gelegen und hat viele Listen an mehreren Stellen ausgelegt. Ich gehe einmal die Woche hin, um sie abzuholen.');
      expect(ergebnis[0].contact, 'Ruft mich einfach an.');
      expect(ergebnis[0].latitude, 52.5001477);
      expect(ergebnis[0].longitude, 13.4181523);
      expect(ergebnis[1].id, 3);
      expect(ergebnis[1].name, 'Zukunft');
      expect(ergebnis[1].street, 'Laskerstraße');
      expect(ergebnis[1].number, '5');
      expect(ergebnis[1].description, 'Immer offen');
      expect(ergebnis[1].contact, 'Ruft mich einfach an.');
      expect(ergebnis[1].latitude, 52.5016524);
      expect(ergebnis[1].longitude, 13.4655402);
    });

    test(
        'createListLocation calls right path and serializes list location correctly',
        () async {
      when(backend.post('service/listlocations/neu', any, any)).thenAnswer(
          (_) => Future.value(trainHttpResponse(
              MockHttpClientResponseBody(), 200, curry36().toJson())));

      await service.createListLocation(curry36());

      verify(backend.post(
          'service/listlocations/neu',
          '{'
              '"id":1,'
              '"latitude":52.4935584,'
              '"longitude":13.3877282,'
              '"street":"Mehringdamm",'
              '"number":"36",'
              '"name":"Curry 36",'
              '"description":"Immer offen",'
              '"contact":"Ruft mich einfach an.",'
              '"initiativenIds":[1]'
              '}',
          any));
    });

    test('createListLocation deserializes action correctly', () async {
      when(backend.post('service/listlocations/neu', any, any)).thenAnswer(
          (_) => Future.value(trainHttpResponse(
              MockHttpClientResponseBody(), 200, curry36().toJson())));

      ListLocation listLocation = curry36();
      listLocation.id = null;

      ListLocation? response = await service.createListLocation(listLocation);

      expect(response!.id, 1);
      expect(response.name, curry36().name);
      expect(response.street, curry36().street);
      expect(response.number, curry36().number);
      expect(response.latitude, curry36().latitude);
      expect(response.longitude, curry36().longitude);
      expect(response.description, curry36().description);
      expect(response.contact, curry36().contact);
      expect(response.initiatives, curry36().initiatives);
    });

    test(
        'saveListLocation (update) calls right path and serializes list location correctly',
        () async {
      when(backend.post('service/listlocations/${curry36().id}', any, any))
          .thenAnswer((_) => Future.value(
              trainHttpResponse(MockHttpClientResponseBody(), 200, null)));

      ListLocation listLocation = curry36();

      listLocation.initiatives.add(DemoInitiativesService.klimaneutral);
      listLocation.contact = "Vali, call me";
      listLocation.description = "Neue Beschreibung";

      await service.saveListLocation(listLocation);

      verify(backend.post(
          'service/listlocations/${curry36().id}',
          '{'
              '"id":1,'
              '"latitude":52.4935584,'
              '"longitude":13.3877282,'
              '"street":"Mehringdamm",'
              '"number":"36",'
              '"name":"Curry 36",'
              '"description":"Neue Beschreibung",'
              '"contact":"Vali, call me",'
              '"initiativenIds":[1,2]'
              '}',
          any));
    });
  });

  group('DemoListLocationService', () {
    var service;
    setUp(() {
      service = DemoListLocationService(initiativesService, userService);
    });

    test('uses DemoBackend', () {
      expect(service.backend is DemoBackend, true);
    });

    test('DemoListLocationService returns list locations', () async {
      List<ListLocation> result =
          await service.getActiveListLocations([1, 2, 3]);
      expect(result.length, 3);

      expect(result[0].id, curry36().id);
      expect(result[0].name, curry36().name);
      expect(result[0].street, curry36().street);
      expect(result[0].number, curry36().number);
      expect(result[0].latitude, curry36().latitude);
      expect(result[0].longitude, curry36().longitude);
      expect(result[0].description, curry36().description);
      expect(result[0].contact, curry36().contact);

      expect(result[1].id, cafeKotti().id);
      expect(result[1].name, cafeKotti().name);
      expect(result[1].street, cafeKotti().street);
      expect(result[1].number, cafeKotti().number);
      expect(result[1].latitude, cafeKotti().latitude);
      expect(result[1].longitude, cafeKotti().longitude);
      expect(result[1].description, cafeKotti().description);
      expect(result[1].contact, cafeKotti().contact);

      expect(result[2].id, zukunft().id);
      expect(result[2].name, zukunft().name);
      expect(result[2].street, zukunft().street);
      expect(result[2].number, zukunft().number);
      expect(result[2].latitude, zukunft().latitude);
      expect(result[2].longitude, zukunft().longitude);
      expect(result[2].contact, zukunft().contact);
    });

    test('creates new list location', () async {
      var response = await service.createListLocation(ListLocation(
          null,
          'Büro',
          'Gneisenaustraße',
          '63',
          'Immer offen',
          'Ruft mich einfach an.',
          52.52116,
          13.41331, []));

      expect(response.id, 4);
    });

    test('updates list location', () async {
      ListLocation listLocation = curry36();

      listLocation.initiatives.add(DemoInitiativesService.expedition);
      var response = await service.saveListLocation(listLocation);

      expect(response, null);
    });
  });
}
