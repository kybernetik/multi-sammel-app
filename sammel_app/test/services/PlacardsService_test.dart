import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sammel_app/model/Placard.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/PlacardsService.dart';
import '../shared/TestdatenVorrat.dart';
import '../shared/mocks.costumized.dart';
import '../shared/mocks.mocks.dart';
import '../shared/mocks.trainer.dart';

void main() {
  final userService = MockUserService();
  MockInitiativesService initiativesService = MockInitiativesService();
  setUp(() {
    reset(userService);
    trainUserService(userService);
  });

  group('PlacardsService', () {
    late MockBackend backend;
    late PlacardsService service;

    setUp(() {
      backend = MockBackend();
      trainBackend(backend);
      trainInitiativesService(initiativesService);
      service = PlacardsService(initiativesService, userService, backend);
    });

    test('loadPlacards calls right path', () async {
      when(backend.post('service/plakate', any, any)).thenAnswer((_) =>
          Future.value(
              trainHttpResponse(MockHttpClientResponseBody(), 200, [])));

      await service.loadPlacards([1, 2, 3]);

      verify(backend.post('service/plakate', any, any));
    });

    test('loadPlacards deserializes Response', () async {
      when(backend.post('service/plakate', any, any)).thenAnswer((_) {
        var placards = [
          placard1().toJson(),
          placard2().toJson(),
          placard3().toJson()
        ];
        return Future.value(
            trainHttpResponse(MockHttpClientResponseBody(), 200, placards));
      });

      var response = await service.loadPlacards([1, 2, 3]);

      expect(response[0].id, 1);
      expect(response[1].id, 2);
      expect(response[2].id, 3);
      expect(response[0].initiative?.id, 1);
      expect(response[1].initiative?.id, 1);
      expect(response[2].initiative?.id, 3);
      expect(response[0].adresse, '12161, Friedrich-Wilhelm-Platz 57');
      expect(response[1].adresse, '12161, Bundesallee 76');
      expect(response[2].adresse, '12161, Goßlerstraße 29');
    });

    test('createPlacard sends serialized placard to right path', () async {
      when(backend.post('service/plakate/neu', any, any)).thenAnswer((_) =>
          Future.value(trainHttpResponse(
              MockHttpClientResponseBody(), 200, placard1().toJson())));

      var placard = placard1();
      placard.id = null;
      await service.createPlacard(placard);

      verify(backend.post(
          'service/plakate/neu',
          '{'
              '"id":null,'
              '"latitude":52.472246,'
              '"longitude":13.327783,'
              '"adresse":"12161, Friedrich-Wilhelm-Platz 57",'
              '"benutzer":11,'
              '"abgehangen":false,'
              '"initiativenId":1'
              '}',
          any));
    });

    test('createPlacard returns placard from database with Id', () async {
      when(backend.post('service/plakate/neu', any, any)).thenAnswer((_) =>
          Future.value(trainHttpResponse(
              MockHttpClientResponseBody(), 200, placard1().toJson())));

      var placard = placard1();
      placard.id = null;
      var response = await service.createPlacard(placard);

      expect(response!.id, 1);
    });

    test('createPlacard returns null on Error', () async {
      when(backend.post('service/plakate/neu', any, any)).thenThrow(Error());

      var response = await service.createPlacard(placard1());

      expect(response, isNull);
    });

    test('deletePlacard calls right path with id', () async {
      when(backend.delete('service/plakate/1', any, any)).thenAnswer((_) =>
          Future.value(
              trainHttpResponse(MockHttpClientResponseBody(), 200, null)));

      await service.deletePlacard(1);

      verify(backend.delete('service/plakate/1', any, any));
    });
  });

  group('DemoPlacardsService', () {
    DemoPlacardsService placardService =
        DemoPlacardsService(initiativesService, userService);

    setUp(() {
      placardService = DemoPlacardsService(initiativesService, userService);
    });

    test('createPlacard stores placard with new id and returns it', () async {
      expect(placardService.placards.length, 3);

      var newPlacard = await placardService.createPlacard(Placard(
          0,
          52.47065,
          13.3285,
          '12161, Bundesallee 129',
          11,
          false,
          DemoInitiativesService.expedition));

      expect(placardService.placards.length, 4);
      expect(placardService.placards[3].id, 4);
      expect(placardService.placards[3].initiative?.id, 1);
      expect(newPlacard.id, 4);
    });

    test('createPlacard adds placard with original id', () {
      expect(placardService.placards.length, 3);

      placardService.createPlacard(Placard(
          10,
          52.47065,
          13.3285,
          '12161, Bundesallee 129',
          11,
          false,
          DemoInitiativesService.klimaneutral));

      expect(placardService.placards.length, 4);
      expect(placardService.placards[3].id, 10);
      expect(placardService.placards[3].initiative?.id, 2);
    });

    test('deletePlacard removes corrrect placard', () {
      expect(placardService.placards.length, 3);

      placardService.deletePlacard(3);

      expect(placardService.placards.length, 2);
      expect(placardService.placards.map((pl) => pl.id), containsAll([1, 2]));
    });

    test('deletePlacard ignores unkown placard', () {
      expect(placardService.placards.length, 3);

      placardService.deletePlacard(4);

      expect(placardService.placards.length, 3);
    });

    test('loadPlacards serves all placards', () async {
      var placards = await placardService.loadPlacards([1, 2, 3]);

      expect(placards.map((pl) => pl.id), containsAll([1, 2, 3]));
    });

    test('loadPlacards serves only placards of one ini', () async {
      var placards = await placardService.loadPlacards([1]);

      expect(placards.map((pl) => pl.initiative?.id), isNot(contains([2, 3])));
    });
  });
}
