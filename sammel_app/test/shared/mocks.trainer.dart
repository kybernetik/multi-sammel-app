import 'dart:async';
import 'dart:ui';

import 'package:easy_localization/src/localization.dart';
import 'package:easy_localization/src/translations.dart';
import 'package:http_server/http_server.dart';
import 'package:mockito/mockito.dart';
import 'package:sammel_app/model/Health.dart';
import 'package:sammel_app/model/TermineFilter.dart';
import 'package:sammel_app/services/GeoService.dart';
import 'package:sammel_app/services/InitiativesService.dart';
import 'package:sammel_app/services/VisitedHouseView.dart';

import '../model/Termin_test.dart';
import '../services/FAQService_test.dart';
import '../shared/mocks.costumized.dart';
import '../shared/mocks.mocks.dart';
import 'TestdatenVorrat.dart';

MockStammdatenService trainStammdatenService(MockStammdatenService mock) {
  when(mock.kieze).thenAnswer((_) =>
      Future.value([ffAlleeNord(), tempVorstadt(), plaenterwald()].toSet()));
  when(mock.regionen).thenAnswer(
      (_) => Future.value([fhainOst(), kreuzbergSued(), koepenick1()].toSet()));
  when(mock.ortsteile).thenAnswer((_) =>
      Future.value([friedrichshain(), kreuzberg(), koepenick()].toSet()));
  when(mock.getKiezAtLocation(any))
      .thenAnswer((_) => Future.value(plaenterwald()));
  return mock;
}

MockUserService trainUserService(MockUserService mock) {
  when(mock.user).thenAnswer((_) => Stream.value(karl()));
  when(mock.userHeaders)
      .thenAnswer((_) async => {'Authorization': 'userCreds'});
  return mock;
}

MockStorageService trainStorageService(MockStorageService mock) {
  when(mock.loadAllStoredEvaluations())
      .thenAnswer((_) => Future.value(List.empty()));
  when(mock.loadContact())
      .thenAnswer((_) => Future.value('Ruft an unter 123456'));
  when(mock.loadAllStoredActionIds()).thenAnswer((_) async => []);
  when(mock.loadMyKiez()).thenAnswer((_) async => []);
  when(mock.loadNotificationInterval()).thenAnswer((_) async => 'nie');
  when(mock.loadFilter()).thenAnswer((_) async => TermineFilter.leererFilter());
  when(mock.loadInitiativeIds()).thenAnswer((_) async => []);
  when(mock.saveContact(any)).thenAnswer((_) async => true);
  when(mock.loadFAQTimestamp()).thenAnswer((_) async => null);
  when(mock.saveFAQ(any)).thenAnswer((_) async => true);
  when(mock.saveFAQTimestamp(any)).thenAnswer((_) async => true);
  when(mock.loadChatChannel(any)).thenAnswer((_) async => null);
  when(mock.saveActionToken(any, any)).thenAnswer((_) async => true);
  when(mock.deleteActionToken(any)).thenAnswer((_) async => true);
  return mock;
}

MockChatMessageService trainChatMessageService(MockChatMessageService mock) {
  final _mockChatChannel = new MockChatChannel();
  when(_mockChatChannel.channelMessages).thenReturn([]);
  when(mock.getTopicChannel('global'))
      .thenAnswer((_) async => _mockChatChannel);
  return mock;
}

MockBackend trainBackend(MockBackend mock) {
  final nullReponse =
      trainHttpResponse(MockHttpClientResponseBody(), 200, null);
  when(mock.post(any, any, any)).thenAnswer((_) => Future.value(nullReponse));
  when(mock.get(any, any)).thenAnswer((_) => Future.value(nullReponse));
  when(mock.delete(any, any, any)).thenAnswer((_) => Future.value(nullReponse));
  when(mock.post('service/benutzer/authentifiziere', any, any)).thenAnswer((_) {
    return Future<HttpClientResponseBody>.value(
        trainHttpResponse(MockHttpClientResponseBody(), 200, true));
  });
  when(mock.getServerHealth()).thenAnswer((_) => Future.value(ServerHealth()));
  return mock;
}

HttpClientResponseBody trainHttpResponse(
    HttpClientResponseBody bodyMock, int status, dynamic content) {
  var clientMock = MockHttpClientResponse();
  when(clientMock.statusCode).thenReturn(status);
  when(bodyMock.response).thenAnswer((_) => clientMock);
  when(bodyMock.body).thenReturn(content);
  return bodyMock;
}

trainTranslation(MockTranslations mock, [Function(Translations)? training]) {
  when(mock.get(any)).thenAnswer((inv) => inv.positionalArguments[0]);
  if (training != null) training(mock);
  return Localization.load(Locale('en'), translations: mock);
}

trainFAQService(MockFAQService mock) {
  when(mock.getSortedFAQ(any)).thenAnswer((_) => Stream.value(testItems));
}

trainInitiativesService(MockInitiativesService mock) {
  when(mock.loadInitiatives()).thenAnswer((_) => Future.value([
        DemoInitiativesService.expedition,
        DemoInitiativesService.klimaneutral,
        DemoInitiativesService.autofrei
      ]));
}

trainTermineService(MockTermineService mock) {
  when(mock.loadActions(any)).thenAnswer((_) async => []);
  when(mock.createAction(any, any))
      .thenAnswer((_) async => TerminTestDaten.einTermin());
  when(mock.deleteAction(any, any)).thenAnswer((_) => true);
}

trainVisitedHousesService(MockVisitedHousesService mock) {
  when(mock.getAllHouses()).thenAnswer((_) => []);
  when(mock.loadVisitedHouses()).thenAnswer((_) async => []);
  when(mock.getVisitedHousesInArea(any))
      .thenReturn(VisitedHouseView(BoundingBox(0, 0, 0, 0), []));
}

trainPlacardsService(MockPlacardsService mock) {
  when(mock.loadPlacards(any)).thenAnswer((_) async => Future.value([]));
}

trainListLocationService(MockListLocationService mock) {
  when(mock.getActiveListLocations(any)).thenAnswer((_) async => []);
}

trainPushManager(MockPushNotificationManager mock) {
  when(mock.pushToken).thenAnswer((_) async => 'Token');
}

trainGeoService(MockGeoService mock) {
  when(mock.getDescriptionToPoint(any)).thenAnswer(
      (_) => Future.value(GeoData('Nightmare', 'Elm Street', '12')));
}
