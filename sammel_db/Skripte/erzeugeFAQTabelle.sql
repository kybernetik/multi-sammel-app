create table FAQ
(
    id            int   unique  not null,
    title         char(120)     not null,
    teaser        text          not null,
    rest          text          null,
    order_nr      double        not null,
    initiative_id  int          null,
    constraint FAQ_pk
        primary key (id),
    constraint FAQ_Initiativen_fk
        foreign key (initiative_id) references Initiativen (id)
            on update cascade
);

create table FAQ_Tags
(
    faq           int           not null,
    tag           char(40)      not null
);

create table FAQ_Timestamp
(
    timestamp     datetime      not null,
    constraint FAQ_pk
        primary key (timestamp)
);