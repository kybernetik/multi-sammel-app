create table Initiativen
(
    id              int auto_increment        primary key,
    longName        varchar(100),
    shortName       varchar(20),
    primaryColor    varchar(10),
    secondaryColor  varchar(10),
    phase           varchar(20),
    websiteUrl      varChar(2000),
    registerSignaturesUrl varChar(2000),
    description     text
);