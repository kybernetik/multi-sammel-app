create table Plakate
(
    id         int auto_increment        primary key,
    latitude   double       not null,
    longitude  double       not null,
    adresse    varchar(120) not null,
    user_id    int          not null,
    abgehangen bool         not null default true,
    initiativen_id  int          null,
    constraint Plakate_Initiativen_fk
        foreign key (initiativen_id) references Initiativen (id)
            on update cascade
);

--alter table Plakate add column (abgehangen bool not null default true)