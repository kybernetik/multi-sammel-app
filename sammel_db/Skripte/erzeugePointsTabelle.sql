create table points
(
    id            int auto_increment primary key,
    Name          varchar(60) not null,
    Strasse       varchar(60),
    Nr            varchar(40),
    Beschreibung  longtext  null,
    Kontakt       text      null,
    Laengengrad   double             not null,
    Breitengrad   double             not null,
    active        bool               not null
);

create table point_initiative
(
    point     int null,
    initiative int null,
    constraint point_initiative_initiativen_fk
        foreign key (initiative) references Initiativen (id)
            on delete cascade,
    constraint point_initiative_points_fk
        foreign key (point) references points (id)
            on delete cascade
)
    comment 'Liste der zu den Soli-Orten zugehörigen Initiativen';

