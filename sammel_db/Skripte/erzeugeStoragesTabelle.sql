create table storages
(
    id            int auto_increment primary key,
    Name          varchar(60)  null,
    Strasse       varchar(60),
    Nr            varchar(40),
    Ausstattung   longtext  null,
    Kontakt       text  null,
    Typ           varchar(80) null,
    Laengengrad   double             not null,
    Breitengrad   double             not null,
    active        bool               not null
);
