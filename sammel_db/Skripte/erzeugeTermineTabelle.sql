create table Termine
(
    id      int auto_increment        primary key,
    beginn  datetime     null,
    ende    datetime     null,
    ort     varchar(120) null,
    typ     varchar(40)  null,
    latitude  double     null,
    longitude  double    null
);

create table Evaluationen
(
    id              int auto_increment      primary key,
    termin_id       int     null,
    user_id         int     null,
    teilnehmer     int     null,
    unterschriften  int     null,
    fuer_alle       bool    not null        default false,
    schaetzung      bool    not null        default false,
    bewertung       int     null,
    stunden         double  null,
    in_bewegung     bool    not null        default false,
    kommentar       text    null,
    beschreibung    text    null,
    sonstiges       text    null,
    ausgefallen     bool    not null        default false,

    constraint Evaluationen_Benutzer_fk
        foreign key (user_id) references Benutzer (id)
            on update cascade
);

create table Evaluation_Typen
(
    evaluation    int           not null,
    ort_typen     char(80)      not null
);


create table TerminDetails
(
    termin_id       int auto_increment        primary key,
    treffpunkt      text     null,
    beschreibung    longtext null,
    kontakt         text     null,
    constraint TerminDetails_Termine_fk
        foreign key (termin_id) references Termine (id)
            on update cascade on delete cascade
)
    comment 'naehere Infos zu Terminen';

create table Termin_Teilnehmer
(
    termin     int null,
    teilnehmer int null,
    constraint Termin_Teilnehmer_Benutzer_fk
        foreign key (teilnehmer) references Benutzer (id)
            on delete cascade,
    constraint Termin_Teilnehmer_Termine_fk
        foreign key (termin) references Termine (id)
            on delete cascade
)
    comment 'Liste der Teilnehmer der Termine';

create table Termin_Initiative
(
    termin     int null,
    initiative int null,
    constraint Termin_Initiative_Initiativen_fk
        foreign key (initiative) references Initiativen (id)
            on delete cascade,
    constraint Termin_Initiative_Termine_fk
        foreign key (termin) references Termine (id)
            on delete cascade
)
    comment 'Liste der zu den Terminen zugehörigen Initiativen';

