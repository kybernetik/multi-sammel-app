#!/bin/bash
files="
_leereDatenbank.sql
erzeugeBenutzertabelle.sql
erzeugeBesuchteHaeuserTabelle.sql
erzeugeInitiativenTabelle.sql
erzeugeFAQTabelle.sql
erzeugePlakateTabelle.sql
erzeugePointsTabelle.sql
erzeugeStoragesTabelle.sql
erzeugePushMessageTabelle.sql
erzeugeSubscriptionTabelle.sql
erzeugeTermineTabelle.sql
erzeugeTokentabelle.sql
erzeugeVorbehalteTabelle.sql
fuelleBenutzerTabelle.sql
fuelleBesuchteHaeuserTabelle.sql
fuelleInitiativenTabelle.sql
fuelleFAQTabelle.sql
fuellePlakateTabelle.sql
fuellePointsTabelle.sql
fuelleStoragesTabelle.sql
fuelleTermineTabelle.sql
fuelleTokentabelle.sql
fuelleVorbehalteTabelle.sql
"
echo "Führe Skripte aus:"
for script in $files
do
  echo -n "-> /home/Skripte/$script "
  mysql -D db -u root --password=$MYSQL_ROOT_PASSWORD < /home/Skripte/$script && echo "✓"
  sleep 0.3
done