package de.kybernetik.database.initiativen

import javax.persistence.*

@Entity
@Table(name = "Initiativen")
class Initiative {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column
    var shortName: String? = null

    @Column
    var longName: String? = null

    @Column
    var primaryColor: String? = null

    @Column
    var secondaryColor: String? = null

    @Column
    var websiteUrl: String? = null

    @Column
    var registerSignaturesUrl: String? = null

    @Column
    var description: String? = null

    @Column
    var phase: String? = null

    @Suppress("unused")
    constructor()

    constructor(id: Long,
                longName: String?,
                shortName: String?,
                primaryColor: String?,
                secondaryColor: String?,
                websiteUrl: String?,
                registerSignaturesUrl: String?,
                description: String?,
    phase: String?) {
        this.id = id
        this.longName = longName
        this.shortName = shortName
        this.primaryColor = primaryColor
        this.secondaryColor = secondaryColor
        this.websiteUrl = websiteUrl
        this.registerSignaturesUrl = registerSignaturesUrl
        this.description = description
        this.phase = phase
    }
}