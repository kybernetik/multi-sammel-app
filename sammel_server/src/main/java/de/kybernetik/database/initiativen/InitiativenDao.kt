package de.kybernetik.database.initiativen

import org.jboss.logging.Logger
import javax.annotation.security.RolesAllowed
import javax.ejb.Stateless
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Stateless
open class InitiativenDao {
    private val LOG = Logger.getLogger(InitiativenDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    @RolesAllowed("app")
    open fun ladeInitiativen(): List<Initiative> {
        LOG.trace("Lade alle Initiativen")
        return entityManager.createQuery("from Initiative", Initiative::class.java).resultList
    }
}