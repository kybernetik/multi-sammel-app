package de.kybernetik.database.listlocations

import javax.persistence.*

@Entity
@Table(name = "points")
class ListLocation() {

    constructor(id: Long, name: String, strasse: String, nr: String, laengengrad: Double?, breitengrad: Double?,  beschreibung: String?, kontakt: String?, initiativenIds: List<Long>?) : this() {
        this.id = id
        this.name = name
        this.strasse = strasse
        this.nr = nr
        this.laengengrad = laengengrad
        this.breitengrad = breitengrad
        this.beschreibung = beschreibung
        this.kontakt = kontakt
        this.initiativenIds = initiativenIds
        this.active = true
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column
    var name: String = ""

    @Column
    var strasse: String = ""

    @Column
    var nr: String = ""

    @Column
    var laengengrad: Double? = null

    @Column
    var breitengrad: Double? = null

    @Column
    var beschreibung: String? = null

    @Column
    var kontakt: String? = null

    @Column
    var active: Boolean? = false

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "point_initiative", joinColumns = [JoinColumn(name = "point")])
    @Column(name = "Initiative")
    var initiativenIds: List<Long>? = emptyList()
}

