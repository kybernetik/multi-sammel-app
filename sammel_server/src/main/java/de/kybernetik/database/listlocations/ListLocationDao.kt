package de.kybernetik.database.listlocations

import de.kybernetik.database.plakate.Plakat
import de.kybernetik.rest.ListLocationFilter
import org.jboss.logging.Logger
import javax.ejb.Stateless
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Stateless
open class ListLocationDao {
    private val LOG = Logger.getLogger(ListLocationDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun getActiveListLocations(filter: ListLocationFilter): List<ListLocation>? {
        LOG.debug("Ermittle Listenorte")

        val filterInis: Boolean = !filter.initiativenIds.isNullOrEmpty()

        var sql = ""

        if (filterInis) {
            sql = "select distinct listLocation from ListLocation listLocation join listLocation.initiativenIds as ini where listLocation.active = true and ini in (:initiativen)"
        } else {
            sql = "from ListLocation where active = true"
        }

        val query = entityManager.createQuery(sql, ListLocation::class.java);

        if(filterInis){
            query.setParameter("initiativen", filter.initiativenIds);
        }

        val listlocations = query.resultList
        LOG.debug("Listenorte gefunden: ${listlocations.map { it.name }}")
        return listlocations
    }

    open fun createListLocation(listLocation: ListLocation): ListLocation {
        LOG.debug("Speichere Soli Ort")

        entityManager.persist(listLocation)
        entityManager.flush()

        LOG.debug("ID von neu erstelltem Soli Ort. ${listLocation.id}")

        return listLocation
    }

    open fun getListLocation(id: Long): ListLocation? {
        LOG.trace("Lade Soli-Ort $id")
        try {
            return entityManager.find(ListLocation::class.java, id)
        } catch (e: IllegalArgumentException) {
            LOG.error(e.message, e)
            return null
        }
    }

    open fun updateListLocation(listLocation: ListLocation): ListLocation? {
        try {
            entityManager.merge(listLocation)
            entityManager.flush();

            return listLocation;
        } catch (e: IllegalArgumentException) {
            LOG.error(e.message, e)
            return null
        }
    }
}
