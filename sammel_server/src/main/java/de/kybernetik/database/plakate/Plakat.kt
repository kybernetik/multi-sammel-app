package de.kybernetik.database.plakate

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "Plakate")
class Plakat: Serializable {

    @Suppress("unused") // Wichtig für JPA
    constructor() {
        latitude = 0.0
        longitude = 0.0
        adresse = ""
        user_id = 0
        abgehangen = false
        initiativen_id = null
    }

    constructor(
        id: Long,
        latitude: Double,
        longitude: Double,
        adresse: String,
        user_id: Long,
        abgehangen: Boolean,
        initiativen_id: Long?
    ) {
        this.id = id
        this.latitude = latitude
        this.longitude = longitude
        this.adresse = adresse
        this.user_id = user_id
        this.abgehangen = abgehangen
        this.initiativen_id = initiativen_id
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column
    var latitude: Double

    @Column
    var longitude: Double

    @Column
    var adresse: String

    @Column
    var user_id: Long

    @Column
    var abgehangen: Boolean

    @Column(name = "initiativen_id")
    var initiativen_id: Long? = null

}