package de.kybernetik.database.plakate

import de.kybernetik.rest.ListLocationFilter
import de.kybernetik.rest.PlakateFilter
import org.jboss.logging.Logger
import java.lang.IllegalArgumentException
import javax.ejb.Stateless
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Stateless
open class PlakateDao {
    private val LOG = Logger.getLogger(PlakateDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun erstellePlakat(plakat: Plakat): Plakat {
        LOG.debug("Speichere neues Plakat in Datenbank")
        entityManager.persist(plakat)
        entityManager.flush()
        LOG.debug("ID von neu erstelltem Plakat: ${plakat.id}")
        return plakat
    }

    open fun ladePlakat(id: Long): Plakat? {
        LOG.trace("Lade einzelnes Plakat $id")
        try {
            return entityManager.find(Plakat::class.java, id)
        } catch (e: IllegalArgumentException) {
            LOG.error(e.message, e)
            return null
        }
    }

    open fun loeschePlakat(plakat: Plakat) {
        LOG.debug("Lösche Plakat ${plakat.id}, in ${plakat.adresse}")
        entityManager.remove(plakat)
    }

    open fun ladeAllePlakate(filter: PlakateFilter): List<Plakat> {
        LOG.trace("Lade alle Plakate")
        val filterInis: Boolean = !filter.initiativenIds.isNullOrEmpty()

        var sql = ""

        if (filterInis) {
            sql = "select distinct plakat from Plakat plakat where plakat.initiativen_id in (:initiativen)"
        } else {
            sql = "from Plakat"
        }

        val query = entityManager.createQuery(sql, Plakat::class.java)

        if(filterInis){
            query.setParameter("initiativen", filter.initiativenIds);
        }

        val plakate = query.resultList
        return plakate
    }
}