package de.kybernetik.database.storages

import javax.persistence.*

@Entity
@Table(name = "storages")
class Storage() {

    constructor(id: Long, name: String?, strasse: String, nr: String, laengengrad: Double?, breitengrad: Double?,  ausstattung: String?, kontakt: String?, typ: String?, initiativen_id: Long?) : this() {
        this.id = id
        this.name = name
        this.strasse = strasse
        this.nr = nr
        this.laengengrad = laengengrad
        this.breitengrad = breitengrad
        this.ausstattung = ausstattung
        this.kontakt = kontakt
        this.typ = typ
        this.active = true
        this.initiativen_id = initiativen_id
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column
    var name: String? = ""

    @Column
    var strasse: String = ""

    @Column
    var nr: String = ""

    @Column
    var laengengrad: Double? = null

    @Column
    var breitengrad: Double? = null

    @Column
    var ausstattung: String? = null

    @Column
    var kontakt: String? = null

    @Column
    var typ: String? = null

    @Column
    var active: Boolean? = false

    @Column(name = "initiativen_id")
    var initiativen_id: Long? = null
}

