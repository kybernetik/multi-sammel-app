package de.kybernetik.database.storages

import de.kybernetik.database.plakate.Plakat
import org.jboss.logging.Logger
import javax.ejb.Stateless
import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Stateless
open class StorageDao {
    private val LOG = Logger.getLogger(StorageDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun getActiveStorages(): List<Storage>? {
        LOG.debug("Ermittle Materiallager")
        val query = "from Storage where active = true"
        val storages = entityManager
                .createQuery(query, Storage::class.java)
                .resultList
        LOG.debug("Materiallager gefunden: ${storages.map { it.name }}")
        return storages
    }
}
