package de.kybernetik.database.termine

import javax.persistence.*

@Entity
@Table(name = "Evaluationen")
class Evaluation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0

    @Column
    var termin_id: Long? = null

    @Column
    var user_id: Long? = null

    @Column
    var teilnehmer: Long? = null

    @Column
    var unterschriften: Long? = null

    @Column
    var fuer_alle: Boolean? = false

    @Column
    var schaetzung: Boolean? = false

    @Column
    var bewertung: Long? = null

    @Column
    var stunden: Double? = null

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "Evaluation_Typen", joinColumns = [JoinColumn(name = "evaluation")])
    @Column(name = "ort_typen")
    var ort_typen: List<String>? = emptyList()

    @Column
    var in_bewegung: Boolean? = false

    @Column
    var beschreibung: String? = null

    @Column
    var kommentar: String? = null

    @Column
    var sonstiges: String? = null

    @Column
    var ausgefallen: Boolean? = null

    @Suppress("unused")
    constructor()

    constructor(id: Long,
                termin_id: Long?,
                user_id: Long?,
                teilnehmer: Long?,
                unterschriften: Long?,
                fuer_alle: Boolean?,
                schaetzung: Boolean?,
                bewertung: Long?,
                stunden: Double?,
                ort_typen: List<String>?,
                in_bewegung: Boolean?,
                beschreibung: String?,
                kommentar: String?,
                sonstiges: String?,
                ausgefallen: Boolean?) {
        this.id = id
        this.termin_id = termin_id
        this.user_id = user_id
        this.unterschriften = unterschriften
        this.fuer_alle = fuer_alle
        this.schaetzung = schaetzung
        this.teilnehmer = teilnehmer
        this.stunden = stunden
        this.ort_typen = ort_typen
        this.in_bewegung = in_bewegung
        this.bewertung = bewertung
        this.kommentar = kommentar
        this.beschreibung = beschreibung
        this.sonstiges = sonstiges
        this.ausgefallen = ausgefallen
    }
}