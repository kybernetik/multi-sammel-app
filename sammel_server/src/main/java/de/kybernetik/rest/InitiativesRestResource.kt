package de.kybernetik.rest

import de.kybernetik.database.initiativen.Initiative
import de.kybernetik.database.initiativen.InitiativenDao
import de.kybernetik.database.listlocations.ListLocation
import de.kybernetik.database.listlocations.ListLocationDao
import org.jboss.logging.Logger
import javax.annotation.security.RolesAllowed
import javax.ejb.EJB
import javax.ejb.Stateless
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.Response

@Path("initiatives")
@Stateless
open class InitiativesRestResource {
    private val LOG = Logger.getLogger(InitiativesRestResource::class.java)

    @EJB
    private lateinit var dao: InitiativenDao

    @GET
    @RolesAllowed("app")
    @Produces("application/json")
    open fun getInitiatives(): Response {
        LOG.info("Lade Initiativen")
        val initiatives = dao.ladeInitiativen()
        return Response
                .ok()
                .entity(initiatives.map { initiative -> InitiativeDto.vonInitiative(initiative) })
                .build()
    }

    data class InitiativeDto(
            var id: Long? = null,
            var longName: String? = null,
            var shortName: String? = null,
            var primaryColor: String? = null,
            var secondaryColor: String? = null,
            var websiteUrl: String? = null,
            var registerSignaturesUrl: String? = null,
            var description: String? = null,
            var phase: String? = null
    )
    {
        fun convertToInitiative(): Initiative {
            return Initiative(
                    id = id ?: 0,
                    longName = longName,
                    shortName = shortName,
                    primaryColor = primaryColor,
                    secondaryColor = secondaryColor,
                    websiteUrl = websiteUrl,
                    registerSignaturesUrl = registerSignaturesUrl,
                    description = description,
                    phase = phase

            )
        }

        companion object {
            fun vonInitiative(it: Initiative) = InitiativeDto(
                    id = it.id,
                    longName = it.longName,
                    shortName = it.shortName,
                    primaryColor = it.primaryColor,
                    secondaryColor = it.secondaryColor,
                    websiteUrl = it.websiteUrl,
                    registerSignaturesUrl = it.registerSignaturesUrl,
                    description = it.description,
                    phase = it.phase
            )
        }
    }
}