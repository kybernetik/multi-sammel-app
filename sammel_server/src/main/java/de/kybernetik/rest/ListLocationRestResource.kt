package de.kybernetik.rest

import de.kybernetik.database.listlocations.ListLocation
import de.kybernetik.database.listlocations.ListLocationDao
import de.kybernetik.shared.FehlenderWertException
import org.jboss.logging.Logger
import javax.annotation.security.PermitAll
import javax.annotation.security.RolesAllowed
import javax.ejb.EJB
import javax.ejb.Stateless
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("listlocations")
@Stateless
open class ListLocationRestResource {
    private val LOG = Logger.getLogger(ListLocationRestResource::class.java)

    @EJB
    private lateinit var dao: ListLocationDao

    @Context
    private lateinit var context: SecurityContext

    @OPTIONS
    @PermitAll
    @Path("actives")
    open fun optionsGetActiveListLocations(): Response {
        return allowCors();
    }

    @POST
    @Path("actives")
    @PermitAll
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    open fun getActiveListLocations(filter: ListLocationFilter?): Response {
        if (System.getProperty("de.kybernetik.listlocations.secret")?.toBoolean() == true) {
            LOG.debug("Lade keine Solidarischen Orte, weil diese geheim gehalten werden")
            return Response.status(200).entity(emptyList<ListLocation>()).build()
        }

        LOG.info("Lade Aktionen mit Filter ${filter?.initiativenIds}");
        val listLocations: List<ListLocation>? = dao.getActiveListLocations(filter ?: ListLocationFilter())
        val listLocationDtos =
                listLocations
                        ?.filter { it.breitengrad != null && it.laengengrad != null }
                        ?.map(ListLocationDto.Companion::convertFromListLocation)
                        ?: emptyList()
        return Response
                .ok()
                .entity(listLocationDtos)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST")
                .header("Access-Control-Allow-Headers", "accept, authorization, content-type, x-requested-with")
                .build()
    }


    @OPTIONS
    @PermitAll
    @Path("neu")
    open fun optionsCreateListLocation(): Response {
        return allowCors();
    }

    @POST
    @Path("neu")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    open fun createListLocation(listLocationDto: ListLocationDto): Response {
        val listLocation: ListLocation

        LOG.info("json list location dto: ${listLocationDto}");

        try {
            listLocation = dao.createListLocation(listLocationDto.convertToListLocation())
        } catch (e: FehlenderWertException) {
            LOG.error(e.message)
            return Response.status(322).entity(RestFehlermeldung(e.message)).build()
        }

        return Response
                .ok()
                .entity(ListLocationDto.convertFromListLocation(listLocation))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "accept, authorization, content-type, x-requested-with")
                .build()
    }

    @POST
    @RolesAllowed("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    open fun updateListLocation(@PathParam("id") id: Long?, listLocationDto: ListLocationDto): Response {
        if (id == null) {
            LOG.warn("Fehlende id in Update-Request für Soli-Ort durch Benutzer*in ${context.userPrincipal.name}")
            return Response.status(422)
                    .entity(RestFehlermeldung("Kein Soli-Ort an den Server gesendet"))
                    .build()
        }

        val listLocation = dao.getListLocation(id)

        if (listLocation == null) {
            LOG.warn("Benutzer*in ${context.userPrincipal.name} versucht unbekannten Soli-Oirt ${id} zu updaten")
            return Response.status(422)
                    .entity(RestFehlermeldung("Kein gültigen Soli-Ort an den Server gesendet"))
                    .build()
        }

        if (listLocationDto.initiativenIds == null && listLocationDto.contact == null && listLocationDto.description == null) {
            LOG.warn("Keine Attribute übergeben, um Soli-Oirt ${id} zu updaten")
            return Response.status(422)
                    .entity(RestFehlermeldung("Keine Attribute übergeben, um Soli-Ort ${id} zu updaten"))
                    .build()
        }

        val newInitiatives = listLocationDto.initiativenIds

        if(!newInitiatives.isNullOrEmpty()) {
            listLocation.initiativenIds = listLocationDto.initiativenIds
        }


        listLocation.kontakt = listLocationDto.contact
        listLocation.beschreibung = listLocationDto.description
        listLocation.name = listLocationDto.name ?: listLocation.name
        listLocation.strasse = listLocationDto.street ?: listLocation.strasse
        listLocation.nr = listLocationDto.number ?: listLocation.nr
        listLocation.laengengrad = listLocationDto.longitude
        listLocation.breitengrad = listLocationDto.latitude


        LOG.info("Update Soli-Ort ${listLocation.initiativenIds} durch Benutzer*in ${context.userPrincipal.name}")
        dao.updateListLocation(listLocation)

        return Response.ok().build()
    }

    data class ListLocationDto(
            var id: Long? = null,
            var name: String? = "",
            var street: String? = "",
            var number: String? = "",
            var latitude: Double? = null,
            var longitude: Double? = null,
            var description: String? = null,
            var contact: String? = null,
            var initiativenIds: List<Long>? = emptyList()
    ) {

        fun convertToListLocation(): ListLocation {
            if (this.latitude == null)
                throw FehlenderWertException("latitude")
            if (this.longitude == null)
                throw FehlenderWertException("longitude")

            return ListLocation(id = id ?: 0, name = name ?: "", strasse = street
                    ?: "", nr = number
                    ?: "", laengengrad = longitude, breitengrad = latitude, beschreibung = description, kontakt = contact, initiativenIds = initiativenIds)
        }


        companion object {
            fun convertFromListLocation(listLocation: ListLocation): ListLocationDto {
                return (ListLocationDto(
                        listLocation.id,
                        listLocation.name,
                        listLocation.strasse,
                        listLocation.nr,
                        listLocation.breitengrad,
                        listLocation.laengengrad,
                        listLocation.beschreibung,
                        listLocation.kontakt,
                        listLocation.initiativenIds
                ))
            }
        }
    }

    open fun allowCors(): Response {
        return Response
                .ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                .header("Allow", "POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "accept, authorization, content-type, x-requested-with")
                .build()
    }
}