package de.kybernetik.rest

// For now this filter only has one attribute, so the class might seem overkill
// but maybe there are filters added in the future
data class PlakateFilter(
        var initiativenIds: List<Long>? = emptyList()
)