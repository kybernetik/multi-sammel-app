package de.kybernetik.rest

import de.kybernetik.database.storages.Storage
import de.kybernetik.database.storages.StorageDao
import javax.annotation.security.PermitAll
import javax.ejb.EJB
import javax.ejb.Stateless
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext

@Path("storages")
@Stateless
open class StoragesRestResource {

    @EJB
    private lateinit var dao: StorageDao

    @Context
    private lateinit var context: SecurityContext

    @OPTIONS
    @PermitAll
    @Path("")
    open fun optionsGetActiveListLocations(): Response {
        return allowCors();
    }

    @GET
    @PermitAll
    @Produces("application/json")
    open fun getStorages(): Response {
        val storages: List<Storage>? = dao.getActiveStorages()
        val storageDtos =
                storages
                        ?.filter { it.breitengrad != null && it.laengengrad != null }
                        ?.map(StorageDto.Companion::convertFromStorage)
                        ?: emptyList()
        return Response
                .ok()
                .entity(storageDtos)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET")
                .header("Access-Control-Allow-Headers", "accept, authorization, content-type, x-requested-with")
                .build()
    }


    data class StorageDto(
            var id: Long? = null,
            var name: String? = "",
            var street: String? = "",
            var number: String? = "",
            var latitude: Double? = null,
            var longitude: Double? = null,
            var equipment: String? = null,
            var contact: String? = null,
            var type: String? = null,
            var initiativeId: Long? = null,
    ) {

        companion object {
            fun convertFromStorage(storage: Storage): StorageDto {
                return (StorageDto(
                        storage.id,
                        storage.name,
                        storage.strasse,
                        storage.nr,
                        storage.breitengrad,
                        storage.laengengrad,
                        storage.ausstattung,
                        storage.kontakt,
                        storage.typ,
                        storage.initiativen_id
                ))
            }
        }
    }

    open fun allowCors(): Response {
        return Response
                .ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, OPTIONS")
                .header("Allow", "GET, OPTIONS")
                .header("Access-Control-Allow-Headers", "accept, authorization, content-type, x-requested-with")
                .build()
    }
}